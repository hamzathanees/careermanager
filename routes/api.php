<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');
//
//Route::get('/jobposts', function (Request $request) {
//	return $request->user();
//})->middleware('auth:api');

Route::group(['prefix' => 'v1', 'middleware' => 'simpleauth'], function() {
	Route::resource('/jobposts', 'Api\JobPostsController', array('only' => array('index')));
	Route::get('/jobpost/{job_post}/applications', 'Api\JobPostsController@applications');
	Route::get('/jobpost/{job_post}/requirements/data', 'Api\JobPostsController@requirementData');


	Route::get('{employer}/jobposts', 'Api\JobPostsController@getEmployerPosts');
//	Route::get('jobpostnames', 'Api\JobPostsController@getPostNames');
});

Route::group(['prefix' => 'v1'], function() {
	Route::get('jobpostnames', 'Api\JobPostsController@getPostNames');
});

