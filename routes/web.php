<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Crypt;

//All the frontend goes here
Route::get('/', function () {
	$jobcategories = \App\JobCategory::all();
    return view('master', compact('jobcategories'));
})->name('homepage');



//Route::get('/sms', function () {
//	$nexmo = app('Nexmo\Client');
//	$nexmo->message()->send([
//		'to' 	=> '94773669783',
//		'from' 	=> '94773669783',
//		'text' 	=> 'Be aware of this message!'
//	]);
//});

Route::get('/api/v1/job_post/slugs', function (){
	return \App\JobPost::all();
});

Route::get('/job_post/user/{user}', ['as' => 'jobpost.slug.user', 'uses' => 'JobPostViewController@getSlugPerUser']);

Route::get('/api/v1/user_info', function (){
	if(isset($_GET["password"]) && isset($_GET["email"])){
		$password = $_GET["password"];
		$email = $_GET["email"];

		if(Auth::attempt(['email' => $email, 'password' => $password])){
			$user = \App\User::where('email', '=', $email)->first();
			return "welcome";
		}else{
			//error
			return 'Wrong email or password being provided!';
		}
	}else{
		return 'email or password not being set';
	}
});

Route::get('/api/v1/job_search/data', function (){
	$job_posts = \App\JobPost::with('job_categories', 'comments', 'applied_by')->get();

	foreach ($job_posts as $job_post)
	{
		$job_seekers[] = $job_post->applied_by;
	}

	foreach ($job_seekers as $job_seeker)
	{
		$resumes[] = $job_seeker;
	}

	return $job_posts;
});
//Route::get('/job_search', ['as' => 'jobpost.search', 'uses' => 'JobPostViewController@getSearch']);


Route::get('/job_post/{slug}', ['as' => 'jobpost.single', 'uses' => 'JobPostViewController@getSingle']);
Route::get('/job_category/{jobcategory}/job_posts', ['as' => 'category.jobposts', 'uses' => 'JobPostViewController@getAllForCategory']);

Route::group(['middleware' => ['simpleauth'], 'prefix' => '/api/v1'], function() {
	Route::resource('/jobposts', 'Api\JobPostsController', array('only' => array('index')));
});


//All the backend goes here

Route::group(['middleware' => ['web', 'auth', 'roles'], 'prefix' => '/admin', 'roles' => ['Admin']], function() {
    Route::get('/dashboard', function () {
        return view('backend.dashboard');
    })->name('admindashboard');
    Route::resource('/jobcategory', 'JobCategoryController');
//    Route::resource('/users', 'UserController');
});



Route::group(['middleware' => ['web', 'auth', 'roles'], 'roles' => ['Admin', 'Employer', 'Job Seeker']], function() {
	Route::resource('/jobpost', 'JobPostController', ['only' => ['index']]);
	//    Route::resource('/users', 'UserController');
});

Route::group(['middleware' => ['web', 'auth', 'roles'], 'roles' => ['Admin', 'Employer']], function() {
	Route::resource('/jobpost', 'JobPostController', ['except' => ['index']]);
	Route::get('/jobpost/{id}/requirements', 'JobPostRequirementController@getPage');
	Route::get('/jobpost/{id}/requirements/data', 'JobPostRequirementController@getRequirements');
	Route::post('/jobpost/{id}/requirements', 'JobPostRequirementController@saveRequirements');
	//    Route::resource('/users', 'UserController');

	//to view rank
	Route::get('/jobpost/{id}/view_rank', 'JobPostController@getRank');

	//delete this view Edu
	Route::get('/jobpost/{id}/view_edu', 'JobPostController@getEdu');
});

Route::group(['middleware' => ['web', 'auth'], 'prefix' => '/jobpost'], function() {
	Route::post('/{jobpost}/comments', 'JobPostCommentController@save', function (){

	})->name('jobpost.comment.save');
});

Route::group(['middleware' => ['web', 'auth']], function() {
	Route::resource('/employer_profiles', 'EmployerProfileController');
	Route::get('/profile/{slug}', ['as' => 'profile.index', 'uses' => 'ProfileController@getProfile']);
	Route::post('/profile/image', ['as' => 'profile.avatar', 'uses' => 'ProfileController@updateAvatar']);
	Route::resource('/job_seeker_profiles', 'JobSeekerProfileController');
//	Route::get('/job_seeker_profiles/{job_seeker_profile}/resume', ['as' => 'job_seeker_profiles.resume', 'uses' => 'JobSeekerProfileController@resume']);
	//same new for above
	Route::get('/job_seeker_profiles/{job_seeker_profile}/resume', ['as' => 'job_seeker_profiles.resume', 'uses' => 'JobSeekerProfileController@getResume']);
	Route::get('/job_seeker_profiles/{job_seeker_profile}/resume/create', ['as' => 'job_seeker_profiles.createresume', 'uses' => 'JobSeekerProfileController@resumeNew']);
	Route::get('/job_seeker_profiles/{job_seeker_profile}/resume/data', ['as' => 'job_seeker_profiles.resume.data', 'uses' => 'JobSeekerProfileController@resumeData']);
	Route::post('/job_seeker_profiles/resume', ['as' => 'job_seeker_profiles.postresume', 'uses' => 'JobSeekerProfileController@resumeSave']);
	Route::post('/apply_job_post/{job_post}/profile/{job_seeker_profile}', ['as' => 'job_seeker_profiles.job_post.apply', 'uses' => 'JobSeekerProfileController@applyJobPost']);

//	Route::post('/apply_job_post/{job_post}/profile/{job_seeker_profile}', ['as' => 'job_seeker_profiles.job_post.edu', 'uses' => 'JobSeekerProfileController@applyJobPostEdu']);
	//delete the below route after testing textmining
	Route::post('/apply_job_post/{job_post}/profile/{job_seeker_profile}/textmining', ['as' => 'job_seeker_profiles.job_post.runtextmining', 'uses' => 'JobSeekerProfileController@runSkillPoints']);
	Route::post('/apply_job_post/{job_post}/profile/{job_seeker_profile}/delete', ['as' => 'job_seeker_profiles.job_post.withdraw', 'uses' => 'JobSeekerProfileController@withdrawJobPost']);
});


Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['web', 'auth', 'roles'], 'prefix' => 'api/v1', 'roles' => ['Admin']], function() {
    Route::resource('/users', 'Api\UserController', ['only' => ['index', 'update', 'store', 'destroy']]);
});


Route::get('/protected', ['middleware' => ['auth', 'admin'], function() {
    return "this page requires that you be logged in and an Admin";
}]);
