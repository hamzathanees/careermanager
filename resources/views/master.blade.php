@inject('countries', 'App\Http\Utilities\Country')
@extends('frontend.layout')
<style media="screen">
    /*Hide the row1 when the user is not logged in*/
    /*#row1{
      display: none;
    }*/

    #search-form
    {
        height: 34px;
        border: 1px solid #999;
        /*margin-left: -30px;*/
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        background-color: #fff;
        overflow: hidden;
    }

    #search-text
    {
        margin-left:3%;
        font-size: 14px;
        color: #ddd;
        border-width: 0;
        background: transparent;
    }
    #search-box input[type="text"]
    {
        width: 70%;
        padding: 7px 0 12px;
        color: #333;
        outline: none;
    }
    #search-button {
        position: relative;
        float: right;
        top: 0;
        right: 0;
        height: 34px;
        width: 18%;
        font-size: 14px;
        color: #fff;
        text-align: center;
        line-height: 32px;
        border-width: 0;
        background-color: #A3D044;
        -webkit-border-radius: 0px 5px 5px 0px;
        -moz-border-radius: 0px 5px 5px 0px;
        border-radius: 0px 5px 5px 0px;
        cursor: pointer;
    }
    /*.services p{
      width: auto;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }*/
    .h3layout{
        color: #A3D044;
        text-transform: uppercase;
        font-family: 'Josefin Slab';
        font-size: 38px;
        font-style: normal;
        font-variant: normal;
        font-weight: 800;
        line-height: 26.4px;
    }
    .partnerimg{
        width: auto;
        height: 150px;

    }

    .hompage-jobposts:hover{
        -moz-box-shadow: 0 0 20px #ccc;
        -webkit-box-shadow: 0 0 20px #ccc;
        box-shadow: 0 0 10px #ccc;
        background-color: #f4f4f4;
        z-index: 10;
    }

    .jobposttitle:hover{
        text-decoration: none;
    }

    #banner{
        background-repeat: no-repeat;
        background-size:100% 100%;
        background-image:url({{ asset('images/employment-banner.jpg') }});
    }

    /*==========  Non-Mobile First Method  ==========*/

    /* Custom, iPhone Retina */
    @media only screen and (min-width : 320px) and (max-width:479px) {

        #banner{
            padding-top: 180px;
        }



    }

    /* Extra Small Devices, Phones */
    @media only screen and (min-width : 480px) {

        #banner{
            padding-top: 180px;
        }

        #banner-search{
            position: inherit; top: -40px;
        }

    }

    /* Small Devices, Tablets */
    @media only screen and (min-width : 768px) {

        #banner{
            padding-top: 160px;
            padding-bottom: 160px;
        }

        #banner-search{
            position: inherit; top: -50px;
        }

    }

    /* Medium Devices, Desktops */
    @media only screen and (min-width : 992px) {
        #banner{
            padding-top: 200px;
        }

        #banner-search{
            position: inherit; top: -30px;
        }
    }

    /* Large Devices, Wide Screens */
    @media only screen and (min-width : 1200px) {
        #banner{
            padding-top: 200px;
        }

        #banner-search{
            position: inherit; top: -30px;
        }
    }

    .jobcatdetails{
        visibility: hidden;
        width: 350px;
        background-color: rgba(163,208,68, 0.5);
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;

        /* Position the tooltip */
        position: absolute;
        z-index: 1;
        top: 20px;
        left: 12%;
    }

    .jobcat:hover .jobcatdetails{
        visibility: visible;
    }

</style>

@section('body')
    <div class="layoutwrapper" id="banner" style="position: relative">
        <div id="pageintro" class="layouthoc layoutclear" style="position: inherit; top: -22px; left: 0px; width: 100%">
            <!-- ################################################################################################ -->
            <div class="container-fluid" id="banner-search">
                <div class="row">
                    <div class="col-sm-3">
                        <form action="" role="form">
                            <div class="form-group">
                                <select class="show-menu-arrow form-control"   title="Country">
                                    <option value="">Country</option>
                                    @foreach($countries::all() as $country)
                                        <option value="{{ $country }}">{{ $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-3">
                        <form action="" role="form">
                            <div class="form-group">
                                <select class="show-menu-arrow form-control" title="Job Category">
                                    <option value="">Job Category</option>
                                    @foreach($jobcategories as $jobcategory)
                                        <option value="{{ $jobcategory->id }}">{{ $jobcategory->name }}</option>
                                    @endforeach
                                    {{--@foreach($cities as $city)--}}
                                    {{--<option>{{ $city->name_en }} | {{ $city->name_si }}</option>--}}
                                    {{--@endforeach--}}
                                </select>
                            </div>
                        </form>
                    </div>

                    <div id='search-box' class="col-sm-6">
                        <form action='/search' id='search-form' method='get' target='_top' role="form">
                            <input id='search-text' name='q' placeholder='Search' type='text'/>
                            <button id='search-button' type='submit'>
                                <span><i class="fa fa-search"></i></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ################################################################################################ -->
        </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="layoutwrapper row2">
        <div class="layouthoc layoutcontainer">
            <!-- ################################################################################################ -->
            <div class="row">
                <div class="col-sm-12" style="top:-45px;">
                    <center>
                        <h3 class="h3layout">Main Partners</h3>
                    </center>
                </div>
                <div class="col-sm-4">
                    <article>
                        <center>
                            <img src="images/demo/gallery/Sheraton.png" alt="" class="partnerimg"/>
                            <h6 id="lh6" class="heading font-x1"><a href="#">Vulputate ullamcorper</a></h6>
                            <p>Rutrum elit nec tincidunt sed aenean aliquet mauris accumsan eget dui id&hellip;</p>
                        </center>
                    </article>
                    <!-- <hr style="color:#A3D044;"> -->
                </div>
                <div class="col-sm-4">
                    <article>
                        <center>
                            <img src="images/demo/gallery/Hilton.png" alt="" class="partnerimg" alt="" />
                            <h6 id="lh6" class="heading font-x1"><a href="#">Lobortis condimentum</a></h6>
                            <p>Dolor eu suscipit facilisis vestibulum vitae semper nisl vivamus a ligula&hellip;</p>
                        </center>
                    </article>
                    <!-- <hr style="color:#A3D044;"> -->
                </div>
                <div class="col-sm-4">
                    <article>
                        <center>
                            <img src="images/demo/gallery/Shangrila.png" class="partnerimg" alt="" />
                            <h6 id="lh6" class="heading font-x1"><a href="#">Malesuada hendrerit</a></h6>
                            <p>Nulla ut imperdiet metus aliquam iaculis mi in tortor accumsan at lobortis&hellip;</p>
                        </center>
                    </article>
                    <!-- <hr style="color:#A3D044;"> -->
                </div>
            </div>
            <!-- ################################################################################################ -->
        </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <style>
        /*.services i{*/
        /*color: #b21c1c;*/
        /*}*/
        /*.services i:hover{*/
        /*color: #b21c1c;*/
        /*}*/
        .services a{
            color: #a3d044;
        }
    </style>

    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="layoutwrapper row3" style="background-color: #252525">
        <section class="layouthoc layoutcontainer layoutclear">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <center>
                        <h4>RECENT JOB POSTS</h4>
                        <a href="{{ url('/job_search') }}" class="btn btn-default"><i class="fa fa-search-plus" aria-hidden="true"></i> See More Job Posts</a>
                    </center>
                    <hr style="color: #FFFFFF">
                </div>
            </div>
        </section>
    </div>

    <div class="layoutwrapper row3">
        <main class="layouthoc layoutcontainer layoutclear">
            <!-- main body -->
            <!-- ################################################################################################ -->
            <div class="row services">
                @foreach($jobcategories as $jobcategory)
                <a href="{{ route('category.jobposts', [$jobcategory]) }}" class="jobcat">
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <p class="">{{substr($jobcategory->name, 0, 20)}} {{ strlen($jobcategory->name) > 20 ? "..." : "" }}</p>
                        <small class="jobcatdetails">{{ $jobcategory->details }}</small>
                    </div>
                </a>
                @endforeach
            </div>
            <div class="layoutclear"></div>
            <!-- ################################################################################################ -->
            <!-- / main body -->
        </main>
    </div>

@stop

@section('pagescripts')
    @include('flash')
@endsection
