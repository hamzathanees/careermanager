@extends('frontend.layout')

@section('pagestyles')
@endsection
@section('body')

    <div class="container" style="padding: 220px 0px; color: black" >
        <div class="row">
            <div class="col-xs-12">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Due Date</th>
                        <th>Comments</th>
                        @if(count(Auth::user()) > 0)
                            @if(Auth::user()->role->name == 'Admin')
                                <th>Action</th>
                            @endif
                            @if(Auth::user()->role->name == 'Job Seeker')
                                <th>View</th>
                            @endif
                            @if(Auth::user()->role->name == 'Employer')
                                <th>View</th>
                            @endif
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($job_posts as $job_post)
                        <tr data-categoryid="{{$job_post->id}}">
                            <td>{{substr($job_post->title, 0, 20)}} {{ strlen($job_post->title) > 20 ? "..." : "" }}</td>
                            <td>{{ substr(strip_tags($job_post->description), 0, 30)}} {{ strlen(strip_tags($job_post->description)) > 30 ? "..." : "" }}</td>
                            <td>{{ substr(date('M j, Y h:ia', strtotime($job_post->due_date)), 0, 50)}} {{ strlen(date('M j, Y h:ia', strtotime($job_post->due_date))) > 50 ? "..." : "" }}</td>
                            <td>{{ $job_post->comments()->count() }}</td>
                            @if(count(Auth::user()) > 0)
                                @if(Auth::user()->role->name == 'Admin')
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{ route('jobpost.show', $job_post->id) }}"><i class="fa fa-eye"></i> View</a>
                                        <a class="btn btn-warning btn-xs" href="{{ route('jobpost.edit', $job_post->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                    </td>
                                @endif
                                @if(Auth::user()->role->name == 'Job Seeker')
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$job_post->slug) }}">{{ url('job_post/'.$job_post->slug) }}</a>
                                    </td>
                                @endif
                                @if(Auth::user()->role->name == 'Employer')
                                    <td>
                                        <a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$job_post->slug) }}">{{ url('job_post/'.$job_post->slug) }}</a>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Due Date</th>
                        <th>Comments</th>
                        @if(count(Auth::user()) > 0)
                            @if(Auth::user()->role->name == 'Admin')
                                <th>Action</th>
                            @endif
                            @if(Auth::user()->role->name == 'Job Seeker')
                                <th>View</th>
                            @endif
                            @if(Auth::user()->role->name == 'Employer')
                                <th>View</th>
                            @endif
                        @endif
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
@stop

@section('pagescripts')
    @include('flash')
    <script>
        $(function () {
            $("#datatable").DataTable({
                scrollX: true,
                lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
            });
        });
    </script>
@endsection