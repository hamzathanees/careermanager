<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    {{--bootstrap, fontawesome, ionicons, sweetalert, jvectormap, html5shiv and respond for IE8--}}
    <link rel="stylesheet" href="{{ asset('build/css/app.css') }}">
    @yield('pagestyles')
    <!-- Theme style --> <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('build/css/backendtheme.css') }}">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Header -->
    @include('backend.partials.header')

    <!-- Left side column. contains the logo and sidebar -->
    @include('backend.partials.aside')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('body')
    </div>
    <!-- /.content-wrapper -->

    <!-- Footer -->
    @include('backend.partials.footer')

</div>
<!-- ./wrapper -->
<script src="{{ asset('build/js/app.js') }}"></script>
@yield('pagescripts')
<!-- AdminLTE App -->
<script src="{{ asset('build/js/backendtheme.js') }}"></script>

</body>
</html>
