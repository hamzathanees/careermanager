@extends('backend.layout')
<!-- Content Header (Page header) -->

@section('body')
    {{--<section class="content-header">--}}
        {{--<h1>--}}
            {{--Dashboard--}}
            {{--<small>Version 2.0</small>--}}
        {{--</h1>--}}
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li class="active">Dashboard</li>--}}
        {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Add Job Category</h4>
                    </div>
                </div>
                <div class="well">
                    <form action="{{ route('jobcategory.store') }}" method="post" id="jobcategoryform">
                        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-xs-4">Name: </label>
                            <div class="col-xs-8">
                                <input type="text" id="name" name="name" class="form-control" placeholder="Job Category" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
									        <strong>{{ $errors->first('name') }}</strong>
							            </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row {{ $errors->has('details') ? ' has-error' : '' }}">
                            <label for="details" class="col-xs-4">Details: </label>
                            <div class="col-xs-8">
                                <textarea name="details" id="details" class="form-control col-xs-12">{{ old('details') }}</textarea>
                                @if ($errors->has('details'))
                                    <span class="help-block">
									        <strong>{{ $errors->first('details') }}</strong>
							            </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-xs-4 pull-right">
                                <button type="submit" class="btn btn-primary btn-sm" style="float: right">Add New</button>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <div class="box box-warning">
                    <div class="box-header with-border">
                        <center><h3 class="box-title">Job Categories</h3></center>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header --><!-- /.box-header -->
                    <div class="box-body">
                        <a href="{{ route('jobcategory.create') }}" style="margin-bottom: 10px" class="btn btn-info pull-left"> Create</a>
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Job Category</th>
                                <th>Description</th>
                                <th>No of Posts</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($jobcategories as $category)
                                <tr class="tablerow" data-categoryid="{{$category->id}}">
                                    <td>{{$category->id}}</td>
                                    <td>{{$category->name}}</td>
                                    <td>{{substr($category->details, 0, 30)}} <span style="color: red">{{ strlen($category->details) > 30 ? "..." : "" }}</span></td>
                                    <td>{{$category->job_posts()->count()}}</td>
                                    <td clexass="tablecolumn row">
                                        <div class="col-md-6">
                                            <a class="btn btn-warning btn-xs" href="{{ route('jobcategory.edit', $category->id) }}"><i class="fa fa-pencil"> Edit</i></a>
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['jobcategory.destroy', $category->id], 'onsubmit' => 'return ConfirmDelete()']) !!}
                                            {!! Form::button('<i class="fa fa-trash"> Delete</i>', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Job Category</th>
                                <th>Description</th>
                                <th>No of Posts</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        &nbsp;
                    </div>
                </div><!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection

@section('pagescripts')
    <script>
        $(function () {
            $("#datatable").DataTable({
                scrollX: true,
                lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
            });
        });
    </script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\JobCategoryRequest', '#jobcategoryform') !!}
    <script>
        $(".deleteForm").on("submit", function(){
            return confirm("Do you want to delete this item?");
        });
    </script>
    {{--Flash function--}}
    @include('flash')

@endsection