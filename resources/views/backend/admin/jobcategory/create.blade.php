@extends('backend.layout')
<!-- Content Header (Page header) -->

@section('body')
    <section class="content-header">
        <h1>
            Dashboard
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box box-warning">
                    <div class="box-header with-border">
                        <center><h3 class="box-title">Add Job Category</h3></center>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div><!-- /.box-header --><!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ route('jobcategory.store') }}" method="post" id="jobcategoryform">
                            <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-xs-4">Name: </label>
                                <div class="col-xs-8">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Job Category" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
									        <strong>{{ $errors->first('name') }}</strong>
							            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row {{ $errors->has('details') ? ' has-error' : '' }}">
                                <label for="details" class="col-xs-4">Details: </label>
                                <div class="col-xs-8">
                                    <textarea name="details" id="details" class="form-control col-xs-12">{{ old('details') }}</textarea>
                                    @if ($errors->has('details'))
                                        <span class="help-block">
									        <strong>{{ $errors->first('details') }}</strong>
							            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 pull-right">
                                    <button type="submit" class="btn btn-primary btn-sm" style="float: right">Add New</button>
                                </div>
                            </div>
                            {{ csrf_field() }}
                        </form>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        &nbsp;
                    </div>
                </div><!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection


@section('pagescripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\JobCategoryRequest', '#jobcategoryform') !!}
@endsection