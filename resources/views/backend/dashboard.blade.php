@extends('backend.layout')
<!-- Content Header (Page header) -->


@section('body')
    {{--<section class="content-header">--}}
        {{--<h1>--}}
            {{--Dashboard--}}
            {{--<small>Version 2.0</small>--}}
        {{--</h1>--}}
        {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li class="active">Dashboard</li>--}}
        {{--</ol>--}}
    {{--</section>--}}

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <a href="">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">User Info</span>
                            <span class="info-box-number">90<small>%</small></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </a>

            <a href="{{ route('jobcategory.index') }}">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red-active"><i class="fa fa-object-group"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Job Category Info</span>
                            <span class="info-box-number">90<small>%</small></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </a>
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">


        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection