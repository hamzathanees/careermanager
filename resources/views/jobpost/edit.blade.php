@extends('backend.layout')

@section('pagestyles')
    <link rel="stylesheet" href="{{ asset('build/css/datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('build/css/select2.min.css') }}">
@endsection

@section('body')

    <div class="container-fluid" style="padding: 50px 20px; color: black" >
        <div class="row">
            <div class="col-xs-12">
                <h3>Update Post</h3>
            </div>
        </div>
        {!! Form::model($jobpost, array('method' => 'PUT', 'route' => ['jobpost.update', $jobpost->id], 'class' => 'form-horizontal', 'id' => 'jobposteditform')) !!}
        <div class="row">
            <div class="col-md-10">
                <hr style="color: lightgrey">
                <div class="form-group">
                    <label class="col-xs-2" for="title">Title: </label>
                    <div class="col-xs-10">
                        <input type="text" name="title" id="title" class="form-control input-lg" value="{{ $jobpost->title }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2" for="category">Categories: </label>
                    <div class="col-sm-10" id="category">
                        <select class="select-multi-cat form-control" name="categories[]" multiple="multiple">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-2" for="description">Job Description: </label>
                    <div class="col-xs-10">
                        <textarea name="description" id="description" class="form-control" rows="10">{{ $jobpost->description }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2" for="due_date">Job Deadline: </label>
                    <div class='input-group date col-sm-10' id='datetimepicker1'>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        <input type='text' name="due_date" id="due_date"  class="form-control" value="{{ $jobpost->due_date }}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Url: </dt>
                        <dd><a href="{{ url('job_post/'.$jobpost->slug) }}">{{ url('job_post/'.$jobpost->slug) }}</a></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Created At: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->created_at)) }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Last Updated: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->updated_at)) }}</dd>
                    </dl>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            {!! Html::linkRoute('jobpost.show', 'Cancel', array($jobpost->id), array('class' => 'btn btn-info btn-block')) !!}
                        </div>
                        <div class="col-xs-6">
                            <button type="submit" class="btn btn-success btn-block"> Save Changes</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('id', $jobpost->id) !!}
            {{ csrf_field() }}
        </div>
        {!! Form::close() !!}
    </div>

@stop

@section('pagescripts')
    <!-- Laravel Javascript Validation -->
    {{--<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\JobPostRequest', '#jobpostform') !!}--}}
    <script type="text/javascript" src="{{ asset('build/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript" src="{{ asset('build/js/datetimepicker.min.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\JobPostRequest', '#jobposteditform') !!}
    @include('flash')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea',
            menubar: false,
            plugins: 'link code'
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format : "YYYY-MM-DD HH:mm",
                minDate: moment().valueOf()
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select-multi-cat").select2();
            $(".select-multi-cat").select2().val({!! json_encode($jobpost->job_categories()->getRelatedIds()) !!}).trigger('change');
        });
    </script>

@endsection
