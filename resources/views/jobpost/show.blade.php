@extends('frontend.layout')

@section('pagestyles')
    <style>
        hr {
            color: lightgrey;
        }
        .container-fluid{
            margin: 0px 10px;
        }
        .label {
            padding: 5px;
        }
    </style>
@endsection

@section('body')

    <div class="container-fluid" style="padding: 220px 0px; color: black" >
        <div class="row">
            <div class="col-md-7 col-xs-12">
                <h3>{{ $jobpost->title }}</h3>


                {!! $jobpost->description !!}

                <hr>

                <div class="tags">
                    @foreach($jobpost->job_categories as $category)
                        <span class="label label-warning">{{ $category->name }}</span>
                    @endforeach
                </div>
            </div>

            <div class="col-md-5 col-xs-12">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Due Date: </dt>
                        <dd>{{ $jobpost->due_date }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Url: </dt>
                        <dd><a href="{{ url('job_post/'.$jobpost->slug) }}">{{ $jobpost->slug }}</a></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Created At: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->created_at)) }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Last Updated: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->updated_at)) }}</dd>
                    </dl>
                    <hr>
                    <div class="row">
                        @if(Auth::user()->role->name == 'Admin' || Auth::user()->id == $jobpost->user_id)
                            <div class="col-xs-6">
                                {!! Html::linkRoute('jobpost.edit', 'Edit', array($jobpost->id), array('class' => 'btn btn-primary btn-block')) !!}
                            </div>
                            <div class="col-xs-6">
                                {!! Form::open(['method' => 'DELETE', 'route' => ['jobpost.destroy', $jobpost->id], 'onsubmit' => 'return ConfirmDelete()']) !!}
                                {!! Form::button('<i class="fa fa-trash"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-block')) !!}
                                {!! Form::close() !!}
                            </div>
                        @endif
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6">
                            @if(Auth::user()->role->name == 'Admin' || Auth::user()->id == $jobpost->user_id)
                                <a href="/jobpost/{{ $jobpost->id }}/requirements" class="btn btn-warning btn-block">Add Job Requirements</a>
                            @endif
                        </div>
                        <div class="col-xs-6">
                            <a href="{{ route('jobpost.index') }}" class="btn btn-default btn-block"><< View All Job Posts</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('pagescripts')
    <!-- Laravel Javascript Validation -->
    {{--<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>--}}
    {{--{!! JsValidator::formRequest('App\Http\Requests\JobPostRequest', '#jobpostform') !!}--}}

    @include('flash')
@endsection