@extends('backend.layout')

@section('pagestyles')
    <link rel="stylesheet" href="{{ asset('build/css/datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('build/css/select2.min.css') }}">
@endsection

@section('body')

    <div class="container" style="padding: 80px 10px; color: black" >
        <div class="row">
            <div class="col-xs-12">
                <h3>Create New Post</h3>
                <hr style="color: lightgrey">
                {!! Form::open(['route' => 'jobpost.store',  'class' => 'form-horizontal', 'id' => 'jobpostform']) !!}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label class="col-sm-2" for="title">Title: </label>
                        <div class="col-sm-10">
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                            <span class="help-block">
									<strong>{{ $errors->first('title') }}</strong>
							    </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
                        <label class="col-sm-2" for="category">Categories: </label>
                        <div class="col-sm-10">
                            <select class="select-multi-cat form-control" id="category" name="categories[]" multiple="multiple">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @if ($errors->has('categories'))
                            <span class="help-block">
                                <strong>{{ $errors->first('categories') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label class="col-sm-2" for="description">Job Description: </label>
                        <div class="col-sm-10">
                            <textarea name="description" id="description" class="form-control" rows="10"></textarea>
                        </div>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group{{ $errors->has('due_date') ? ' has-error' : '' }}">
                        <label class="col-sm-2" for="due_date">Job Deadline: </label>
                        <div class='input-group date col-sm-9' id='datetimepicker1'>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                            <input type='text' name="due_date" id="due_date"  class="form-control" />
                        </div>
                        @if ($errors->has('due_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('due_date') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-block')) }}
                        </div>
                        <div class="col-sm-6">
                            <a href="{{ route('jobpost.index') }}" class="btn btn-default btn-block"><< View All Job Posts</a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop

@section('pagescripts')
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('build/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script type="text/javascript" src="{{ asset('build/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('build/js/datetimepicker.min.js')}}"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea',
            themes: "modern",
            custom_undo_redo_levels: 10,
            menubar: false,
            plugins: 'link code textcolor charmap colorpicker print preview anchor insertdatetime ' +
            'table contextmenu paste advlist lists emoticons',
            toolbar: 'undo redo | insert | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify ' +
            '| bullist numlist outdent indent table | forecolor backcolor emoticons | preview'
        });
    </script>
    {!! JsValidator::formRequest('App\Http\Requests\JobPostRequest', '#jobpostform') !!}
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format : "YYYY-MM-DD HH:mm:ss",
                minDate: moment().valueOf()
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select-multi-cat").select2();
        });
    </script>
@endsection