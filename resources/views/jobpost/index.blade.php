@extends('frontend.layout')

@section('pagestyles')
@endsection
@section('body')

    <div class="container" style="padding: 220px 0px; color: black" >
        <div class="row">
            <div class="col-xs-4">
                <a href="{{ route('jobpost.create') }}" style="margin-bottom: 10px" class="btn btn-info pull-left"> Create</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Due Date</th>
                        <th>Comments</th>
                        @if(Auth::user()->role->name == 'Admin')
                            <th>Action</th>
                        @endif
                        @if(Auth::user()->role->name == 'Job Seeker')
                            <th>View</th>
                        @endif
                        @if(Auth::user()->role->name == 'Employer')
                            <th>View</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($jobposts as $jobpost)
                        <tr data-categoryid="{{$jobpost->id}}">
                            <td>{{substr($jobpost->title, 0, 20)}} {{ strlen($jobpost->title) > 20 ? "..." : "" }}</td>
                            <td>{{ substr(strip_tags($jobpost->description), 0, 30)}} {{ strlen(strip_tags($jobpost->description)) > 30 ? "..." : "" }}</td>
                            <td>{{ substr(date('M j, Y h:ia', strtotime($jobpost->due_date)), 0, 50)}} {{ strlen(date('M j, Y h:ia', strtotime($jobpost->due_date))) > 50 ? "..." : "" }}</td>
                            <td>{{ $jobpost->comments()->count() }}</td>
                            @if(Auth::user()->role->name == 'Admin')
                            <td>
                                <a class="btn btn-primary btn-xs" href="{{ route('jobpost.show', $jobpost->id) }}"><i class="fa fa-eye"></i> View</a>
                                <a class="btn btn-warning btn-xs" href="{{ route('jobpost.edit', $jobpost->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                {{--{!! Form::open(['method' => 'DELETE', 'route' => ['frontendjobpost.destroy', $frontendjobpost->id], 'onsubmit' => 'return ConfirmDelete()']) !!}--}}
                                {{--{!! Form::button('<i class="fa fa-trash"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}--}}
                                {{--{!! Form::close() !!}--}}
                            </td>
                            @endif
                            @if(Auth::user()->role->name == 'Job Seeker')
                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$jobpost->slug) }}">{{ url('job_post/'.$jobpost->slug) }}</a>
                                </td>
                            @endif
                            @if(Auth::user()->role->name == 'Employer')
                                <td>
                                    <a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$jobpost->slug) }}">{{ url('job_post/'.$jobpost->slug) }}</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Due Date</th>
                        <th>Comments</th>
                        @if(Auth::user()->role->name == 'Admin')
                            <th>Action</th>
                        @endif
                        @if(Auth::user()->role->name == 'Job Seeker')
                            <th>View</th>
                        @endif
                        @if(Auth::user()->role->name == 'Employer')
                            <th>View</th>
                        @endif
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

@stop

@section('pagescripts')
    @include('flash')
    <script>
        $(function () {
            $("#datatable").DataTable({
                scrollX: true,
                lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
            });
        });
    </script>
@endsection