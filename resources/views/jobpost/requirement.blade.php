@extends('frontend.layout')

@section('pagestyles')
    <style>
        li > p {
            padding-right: 5px;
        }
        .remove
        {
            float: right;
        }
        .list-group-item{

            background-color: #F5F5F5;
        }
        .removeMulti
        {
            position:absolute;
            top:25%;
            right:2%;
        }
        .oldresume
        {
            color: darkred;
        }
    </style>
@endsection

@section('body')
    <div class="container-fluid" style="padding: 220px 10px 20px 10px; color: black" >
        <div class="row" id="app-6">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Your Requirements Here!</div>
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin: 0 10px">
                            <div class="form-group" id="profile">
                                <label for="skills" >Skills Required:</label>
                                <select class="form-control" v-model="newSkill">
                                    @foreach(\App\Skill::all() as $skill)
                                        <option style="margin-top: 6px">{{ $skill->name }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" id="experience" class="form-control" v-model="newSkill" placeholder="Add to Skill"/>--}}
                                <button class="btn btn-xs btn-success" v-on:click="addNew" style="margin-top: 6px;">Add To Skill List</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="experience" >Minimum Experiences:</label>
                                <input type="text" id="experience" class="form-control" v-model="newExp" placeholder="Experience in Months">
                                {{--<textarea rows="5" cols="10" id="experience" class="form-control" v-model="newExp" placeholder="Add Experience"></textarea>--}}
                                <button class="btn btn-xs btn-success" v-on:click="addNewExp" style="margin-top: 6px;">Add To Experince List</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="education" >Minimum Education:</label>
                                <select class="form-control" v-model="newEdu">
                                    @foreach(\App\EducationLevel::all() as $education)
                                        <option style="margin-top: 6px">{{ $education->name }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" id="education" class="form-control" v-model="newEdu" placeholder="Education level">--}}
                                {{--<textarea rows="5" cols="10" id="education" class="form-control" v-model="newEdu" placeholder="Add Education"></textarea>--}}
                                <button class="btn btn-xs btn-success" v-on:click="addNewEdu" style="margin-top: 6px;">Add To Education List</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Skills: </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="skill-item"
                                    v-for="(skill, index) in requirement_data.skills"
                                    v-bind:title="skill.name"
                                    v-on:remove="requirement_data.skills.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>

                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Experiences (Months): </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="exp-item"
                                    v-for="(exp, index) in requirement_data.experiences"
                                    v-bind:title="exp.months"
                                    v-on:remove="requirement_data.experiences.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>

                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Education: </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="edu-item"
                                    v-for="(edu, index) in requirement_data.education"
                                    v-bind:title="edu.name"
                                    v-on:remove="requirement_data.education.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>
                        <div>
                            <input type="hidden" id="token" name="_token" value="{{{ csrf_token() }}}" />
                            <button class="btn btn-sm btn-danger" style="float: right" v-on:click="saveRequirements">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('pagescripts')
    <script src="{{ asset('build/js/vue.min.js') }}"></script>
    <script src="{{ asset('build/js/vue-resource.min.js') }}"></script>
    <script>
        // get csrf token
        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

        Vue.component('skill-item', {
            template: '\
                <li>\
                  <p>@{{ title }}</p>\
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                </li>\
                ',
            props: ['title']
        });

        Vue.component('exp-item', {
            template: '\
                <li style="margin-bottom: 4px">\
                  <p>@{{ title }}</p>\
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                </li>\
                ',
            props: ['title']
        });

        Vue.component('edu-item', {
            template: '\
                <li>\
                  <p>@{{ title }}</p>\
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                </li>\
                ',
            props: ['title']
        });

        //        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');
        new Vue({
            el: '#app-6',
            data: {
                requirement_data: {
                    skills: [ ],
                    experiences: [ ],
                    education: [ ],
                    references: [ ]
                },
                newSkill: '',
                newEdu: '',
                newExp: ''
            },
            mounted: function() {
                this.fetchData();
            },

            methods: {
                fetchData: function () {
                    this.$http.get('/jobpost/' + {!! $jobpost->id !!} + '/requirements/data').
                    then(function (response) {
                        if(response.body.education != null){
                            this.requirement_data.education = response.body.education;
                        }
                        if(response.body.experiences != null){
                            this.requirement_data.experiences = response.body.experiences;
                        }
                        if(response.body.skills != null){
                            this.requirement_data.skills = response.body.skills;
                        }
                        console.log(response.body);
                    });
                },
                addNew: function () {
                    if (this.newSkill != '') {
//                        this.skills.push(this.newSkill)
                        this.requirement_data.skills.push({name: this.newSkill});
                        this.newSkill = ''
                    }
                },
                addNewExp: function () {
                    if (this.newExp != '' && this.newExpDate != ''){
//                        Object.assign(this.experiences, this.newExp, this.newExpDate)
//                        this.experiences.push({name: this.newExp , date: this.newExpDate});
                        this.requirement_data.experiences.push({months: this.newExp});
                        this.newExp = ''
                    }
                },

                addNewEdu: function () {
                    if (this.newEdu != ''){
//                        this.education.push(this.newEdu)
                        this.requirement_data.education.push({name: this.newEdu});
                        this.newEdu = ''
                    }
                },

                saveRequirements: function () {
                    this.$http.post('/jobpost/' + {!! $jobpost->id !!} + '/requirements', this.requirement_data)
                        .then(response => {
                            this.newSkill = ''
                            this.newExp = ''
                            this.newEdu = ''
                            swal(
                                'Hooray!!',
                                'Success! Resume successfully Updated!',
                                'success'
                            );
                            console.log(response.requirement_data)
                        }).catch(response => {
                        let errors = response.body;
                        console.log(errors);
                    })
                }
            }
        });

    </script>
    @include('flash')
@endsection
