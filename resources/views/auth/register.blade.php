@extends('auth.layout.plain')
@section('formbox')

    <div id="signup-box" class="signup-box visible widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header green lighter bigger">
                    <i class="ace-icon fa fa-user-plus blue"></i>
                    New User Registration
                </h4>

                <div class="space-6"></div>
                <p> Enter your details to begin: </p>

                <form role="form" method="POST" action="{{ url('/register') }}">
                    {!! csrf_field() !!}
                    <fieldset>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" />
								<i class="ace-icon fa fa-envelope"></i>
							</span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
									</span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Username"  />
								<i class="ace-icon fa fa-user"></i>
							</span>
                                @if ($errors->has('name'))
                                    <span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
									</span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="password" name="password" class="form-control" placeholder="Password" />
								<i class="ace-icon fa fa-lock"></i>
							</span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
											<strong>{{ $errors->first('password') }}</strong>
									</span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="password" name="password_confirmation" class="form-control" placeholder="Repeat password" />
								<i class="ace-icon fa fa-retweet"></i>
							</span>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
											<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                            <select class="selectpicker form-control" title="Select Role" name="role" data-style="btn-success" id="selectrole">
                                <option value="2">Job Seeker</option>
                                <option value="3">Employer</option>
                            </select>

                            @if ($errors->has('role'))
                                <span class="help-block">
                    <strong>{{ $errors->first('role') }}</strong>
                </span>
                            @endif
                        </div>

                        <label class="block">
                            <input type="checkbox" class="ace" />
                            <span class="lbl">
							I accept the
							<a href="#">User Agreement</a>
						</span>
                        </label>

                        <div class="space-24"></div>

                        <div class="clearfix">
                            <button type="reset" class="btn btn-sm btn-primary">
                                <i class="ace-icon fa fa-refresh"></i>
                                <span class="bigger-110">Reset</span>
                            </button>

                            <button type="submit" class="btn btn-sm btn-success">
                                <span class="bigger-110">Register</span>

                                <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                            </button>
                        </div>
                    </fieldset>
                </form>
                <div class="space-12"></div>
                <center>
                    <div>
                        <a href="/" class="btn btn-success">
                            Go Back Home
                            <i class="ace-icon fa fa-home"></i>
                        </a>
                    </div>
                </center>
            </div>

            <div class="toolbar center">
                <a href="/login" data-target="#login-box" class="back-to-login-link">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Back to login
                </a>
            </div>
        </div><!-- /.widget-body -->
    </div><!-- /.signup-box -->

@stop
