<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Career Manager-Login</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- CSS links -->
    <link rel="stylesheet" href="{{ asset('build/css/app.css') }}">

    <!-- ace styles -->
    <link rel="stylesheet" href="{{ asset('build/css/auththeme.css') }}">

</head>

<body class="login-layout light-login">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i class="ace-icon fa fa-briefcase green"></i>
                            <span class="red">Career</span>
                            <span class="grey" id="id-text2">Manager</span>
                        </h1>
                        <h4 class="blue" id="id-company-text">Innovative Recruitment Platform</h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        @yield('formbox')
                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- JAVASCRIPT links-->
<script src="{{ asset('build/js/app.js') }}"></script>

@yield('pagescripts')

<!-- inline scripts related to this page -->
</body>
</html>
