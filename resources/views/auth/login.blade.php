@extends('auth.layout.plain')
<style>
    #loginbtn{
        background-color: red;
        color: red;
    }
</style>

@section('formbox')
    <div id="login-box" class="login-box visible widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header blue lighter bigger">
                    <i class="ace-icon fa fa-coffee green"></i>
                    Please Enter Your Information
                </h4>

                <div class="space-6"></div>

                @if (session('status'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ session('warning') }}
                    </div>
                @endif

                <form class="form-horizontal" id="loginform" role="form" method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}
                    <fieldset>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" />
								<i class="ace-icon fa fa-envelope"></i>
							</span>
                            </label>
                            @if ($errors->has('email'))
                                <span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
							    </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="block clearfix">
							<span class="block input-icon input-icon-right">
								<input type="password" name="password" class="form-control" placeholder="Password" />
								<i class="ace-icon fa fa-lock"></i>
							</span>
                            </label>
                            @if ($errors->has('password'))
                                <span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
							</span>
                            @endif
                        </div>

                        <div class="space"></div>

                        <div class="clearfix">
                            <label class="inline">
                                <input type="checkbox" class="ace" />
                                <span class="lbl"> Remember Me</span>
                            </label>

                            <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                <i class="ace-icon fa fa-key"></i>
                                <span class="bigger-110">Login</span>
                            </button>
                        </div>

                        <div class="space-8"></div>
                    </fieldset>
                </form>

                <center>
                    <div>
                        <a href="/" class="btn btn-success">
                            Go Back Home
                            <i class="ace-icon fa fa-home"></i>
                        </a>
                    </div>
                    <div class="space-8"></div>
                </center>

                <div class="social-or-login center">
                    <span class="bigger-110">Or Login Using</span>
                </div>

                <div class="space-6"></div>

                <div class="social-login center">
                    <a class="btn btn-primary" href="redirect">
                        <i class="ace-icon fa fa-facebook"></i>
                    </a>

                    <a class="btn btn-info">
                        <i class="ace-icon fa fa-twitter"></i>
                    </a>

                    <a class="btn btn-danger">
                        <i class="ace-icon fa fa-google-plus"></i>
                    </a>
                </div>
            </div><!-- /.widget-main -->

            <div class="toolbar clearfix">
                <div>
                    <a href="{{ url('/password/reset') }}" data-target="#forgot-box" class="forgot-password-link">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        I forgot my password
                    </a>
                </div>
                <div>
                    <a href="/register" data-target="#signup-box" class="user-signup-link">
                        I want to register
                        <i class="ace-icon fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>

        </div><!-- /.widget-body -->
    </div><!-- /.login-box -->
@stop


@section('pagescripts')
    @include('flash')
@endsection