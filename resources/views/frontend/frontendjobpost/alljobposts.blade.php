@extends('frontend.layout')

@section('pagestyles')
    <style>

    </style>
@endsection

@section('body')

    <div class="container" style="padding: 160px 10px; color: black" >
        <div class="row">
            <div class="col-md-12">
                <h3>{{ $jobpost->title }}</h3>
                <p>{{ $jobpost->description }}</p>
            </div>
            <div class="col-md-12">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Due Date: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->due_date)) }}</dd>

                        <dt>Categories: </dt>
                        <dd>
                            @foreach($jobpost->job_categories as $category)
                                <span class="label label-danger">{{ $category->name }}</span>
                            @endforeach
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

@stop

@section('pagescripts')
@endsection