{!! Form::open(['route' => ['jobpost.comment.save', $jobpost->id],  'class' => 'form-horizontal', 'id' => 'jobpostcommentform']) !!}

    @if(isset($parent))
        <input type="hidden" name="parent_id" value="{{ $parent }}">
    @endif

    <div class="form-group">
        <div class="col-md-4 col-sm-8 col-xs-10">
            <small style="color: #0c0c0c">{{ Auth::user()->name }}</small>
            <textarea name="comment" id="comment" class="form-control" rows="3"></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-4 col-md-pull-1 col-sm-6 col-sm-pull-1 col-xs-8 col-xs-offset-1">
            {{ Form::submit('Reply', array('class' => 'btn btn-primary btn-sm')) }}
        </div>
    </div>


{!! Form::close() !!}