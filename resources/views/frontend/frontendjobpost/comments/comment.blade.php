<style>
    .panel-shadow {
        box-shadow: rgba(0, 0, 0, 0.1) 2px 2px 2px;
    }
    .panel-white {
        border: 1px solid #dddddd;
    }
    .panel-white  .panel-heading {
        color: #333;
        background-color: #fff;
        border-color: #ddd;
    }
    .panel-white  .panel-footer {
        background-color: #fff;
        border-color: #ddd;
    }

    .post .post-heading {
        height: 95px;
        padding: 20px 15px;
    }
    .post .post-heading .avatar {
        width: 60px;
        height: 60px;
        display: block;
        margin-right: 15px;
    }
    .post .post-heading .meta .title {
        margin-bottom: 0;
    }
    .post .post-heading .meta .title a {
        color: black;
    }
    .post .post-heading .meta .title a:hover {
        color: #aaaaaa;
    }
    .post .post-heading .meta .time {
        margin-top: 8px;
        color: #999;
    }
    .post .post-image .image {
        width: 100%;
        height: auto;
    }
    .post .post-description {
        padding: 15px;
    }
    .post .post-description p {
        font-size: 14px;
    }
    .post .post-description .stats {
        margin-top: 20px;
    }
    .post .post-description .stats .stat-item {
        display: inline-block;
        margin-right: 15px;
    }
    .post .post-description .stats .stat-item .icon {
        margin-right: 8px;
    }
    .post .post-footer {
        border-top: 3px solid #ddd;
        padding: 15px;
    }
    .post .post-footer .input-group-addon a {
        color: #454545;
    }
</style>

<?php
$nestedcomment = \App\JobPostComment::find($comment->parent_id);
?>
<div class="col-sm-8">
    <div class="panel panel-white post panel-shadow">
        <div class="post-heading">
            <div class="pull-left image">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
            </div>
            <div class="pull-left meta">
                <div class="title h5">
                    <a href="#"><b>{{ $comment->user->name }}</b></a>
                </div>
                <h6 class="text-muted time">{{ date('M j, Y h:ia', strtotime($comment->created_at))}}</h6>
                <h6><small><i class="fa fa-reply"></i> : {{ $comment->parent_id == '' ? $jobpost->user->name : $nestedcomment->user->name }}</small></h6>
            </div>
        </div>
        <div class="post-description">
            <p>{{ $comment->comment }}</p>
            <div class="stats">
                <a href="#" class="btn btn-default stat-item">
                    <i class="fa fa-thumbs-up icon"></i>2
                </a>
                <a href="#" class="btn btn-default stat-item">
                    <i class="fa fa-thumbs-down icon"></i>12
                </a>
            </div>
        </div>
    </div>
    <div class="post-footer">
        @include('frontend.frontendjobpost.comments.commentform', ['parent' => $comment->id])
    </div>
    {{--<div class="well">--}}
        {{--<div class="row commentform">--}}
            {{--@include('frontend.frontendjobpost.comments.commentform', ['parent' => $comment->id])--}}
        {{--</div>--}}
    {{--</div>--}}
</div>


@if(isset($comments[$comment->id]))
        @include('frontend.frontendjobpost.comments.commentlist', ['collection' =>  $comments[$comment->id]])
@endif