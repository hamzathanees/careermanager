@extends('frontend.layout')

@section('pagestyles')
    <style>
        .media-object{
            width: 64px;
            height: 64px;
        }

        #replyform{
            display: none;
        }
    </style>
@endsection

@section('body')

    <div class="container" style="padding: 160px 0px 0px 0px; color: black" >
        <div class="row">
            <div class="col-md-12">
                @if(Auth::user()->id == $jobpost->user_id || Auth::user()->role->name == 'Admin')
                    <a href="/jobpost/{{$jobpost->id}}" class="btn  btn-danger">Manage Job Post</a>
                    <a style="float: right" href="/jobpost/{{$jobpost->id}}/view_rank" class="btn btn-primary">View Rank</a>

                @endif
                <h3>{{ $jobpost->title }}</h3>
                {!! $jobpost->description !!}
            </div>
            <div class="col-md-12">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Due Date: </dt>
                        <dd>{{ date('M j, Y h:ia', strtotime($jobpost->due_date)) }}</dd>
                        <dt>Skills: </dt>
                        <dd>
                            @if($skills != null)
                                @foreach($skills as $skill)
                                    <span class="label label-success">{{ $skill['name'] }}</span>
                                @endforeach
                            @endif
                        </dd>

                        <dt>Education: </dt>
                        <dd>
                            @if($educations != null)
                                @foreach($educations as $education)
                                    <span class="label label-warning">{{ $education['name'] }}</span>
                                @endforeach
                            @endif
                        </dd>

                        <dt>Experience: </dt>
                        <dd>
                            @if($experiences != null)
                                @foreach($experiences as $experience)
                                    <span class="label label-primary">{{ $experience['months'] }} months</span>
                                @endforeach
                            @endif
                        </dd>

                        <dt>Categories: </dt>
                        <dd>
                            @foreach($jobpost->job_categories as $category)
                                <span class="label label-danger">{{ $category->name }}</span>
                            @endforeach
                        </dd>
                    </dl>
                    @if(Auth::user()->role->name == 'Job Seeker')
                        @if (!Auth::user()->job_seeker_profile->applies->contains($jobpost->id))
                            {!! Form::open(['method' => 'POST', 'route' => ['job_seeker_profiles.job_post.apply', $jobpost->id, Auth::user()->job_seeker_profile->id]]) !!}
                            {!! Form::button('<i class="fa fa-envelope"></i> Apply', array('type' => 'submit', 'class' => 'btn btn-success btn-block')) !!}
                            {!! Form::close() !!}
                        @else
                            {!! Form::open(['method' => 'POST', 'route' => ['job_seeker_profiles.job_post.withdraw', $jobpost->id, Auth::user()->job_seeker_profile->id]]) !!}
                            {!! Form::button('<i class="fa fa-trash"></i> Withdaw Application', array('type' => 'submit', 'class' => 'btn btn-danger btn-block')) !!}
                            {!! Form::close() !!}
                        @endif
                        {{--<a class="btn btn-primary" href="/apply_job_post/{{ $jobpost->id }}/profile/{{ Auth::user()->job_seeker_profile->id }}">Apply</a>--}}

                        {{--{!! Form::open(['method' => 'POST', 'route' => ['job_seeker_profiles.job_post.runtextmining', $jobpost->id, Auth::user()->job_seeker_profile->id]]) !!}--}}
                        {{--{!! Form::button('<i class="fa fa-envelope"></i> Run Text Mining', array('type' => 'submit', 'class' => 'btn btn-success btn-block')) !!}--}}
                        {{--{!! Form::close() !!}--}}

                        {{--{!! Form::open(['method' => 'POST', 'route' => ['job_seeker_profiles.job_post.edu', $jobpost->id, Auth::user()->job_seeker_profile->id]]) !!}--}}
                        {{--{!! Form::button('<i class="fa fa-envelope"></i> getEdu', array('type' => 'submit', 'class' => 'btn btn-success btn-block')) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <h5>Comments: <i class="fa fa-comment"></i> {{ $jobpost->comments()->count() }}</h5>
                    </div>
                    <div class="col-md-12 well">
                        @if($jobpost->comments()->count() > 0)
                            @include('frontend.frontendjobpost.comments.commentlist', ['collection' => $comments['root']])
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h6 style="color: #0c0c0c">Leave a comment: </h6>
        <div class="row">
            <div class="well">
                @include('frontend.frontendjobpost.comments.commentform')
            </div>
        </div>
    </div>

@stop

@section('pagescripts')
    @include('flash')
@endsection