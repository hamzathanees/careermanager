<!DOCTYPE html>
<html>
<head>
    <title>CareerManager</title>
    <meta charset="utf-8">
    {{--Favicon Image--}}
    <link rel="shortcut icon" type="image/png" href="http://www.germanystartupjobs.com/wp-content/themes/startupjobs/images/apply-with-cv.png"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    {{--bootstrap, fontawesome, ionicons, sweetalert--}}
    <link rel="stylesheet" href="{{ asset('build/css/app.css') }}">

    <!-- frontend theme -->
    <link rel="stylesheet" href="{{ asset('build/css/frontendtheme.css') }}">


    @yield('pagestyles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="layoutbody" id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div>
    <nav class="navbar navbar-fixed-top">
        <div class="layoutwrapper row0" id="row0" style="padding-left:5px;padding-right:5px;">
            <div id="topbar" class="layouthoc layoutclear">
                <!-- ################################################################################################ -->
                <div class="fl_left">
                    <ul class="layoutnospace layoutul">
                        <li class="layoutli"><a href="{{ url('/') }}"><i class="fa fa-2x fa-home"></i></a></li>
                        <li class="layoutli"><a href="{{ url('/') }}" style="color: white; font-weight: bolder; font-size: medium">Career<span style="color: #A3D044"> Manager</span></a></li>
                    </ul>
                </div>
                <div class="fl_right">
                    <ul class="layoutnospace layoutul">
                        <li class="layoutli"><i class="fa fa-phone"></i>+94 773669783</li>
                        <li class="layoutli"><i class="fa fa-envelope-o"></i>info@careermanager.com</li>
                    </ul>
                </div>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="layoutwrapper row1" style="padding-left:5px;padding-right:5px;height:80px; margin:0 auto;">
            <header id="header" class="layouthoc layoutclear">
                <!-- ################################################################################################ -->
                <!-- ################################################################################################ -->
                <nav id="mainav" class="fl_right">
                    <ul class="layoutclear layoutul">
                        <li class="layoutli"><a href="#" class="drop"><i class="fa fa-clipboard" aria-hidden="true"></i> Job Posts</a>
                            <ul class="layoutul">
                                @if (Auth::guest() || Auth::user()->role->name == 'Job Seeker')
                                    <li class="layoutli"><a href="{{ url('/jobpost') }}">View Job Posts</a></li>
                                @elseif(Auth::user()->role->name == 'Admin')
                                    <li class="layoutli"><a href="{{ url('/jobpost') }}">View Job Posts</a></li>
                                    <li class="layoutli"><a href="{{ url('/view-posts') }}">Manage Job Categories</a></li>
                                    <li class="layoutli"><a href="{{ url('/jobpost/create') }}">Create Job Post</a></li>
                                @elseif(Auth::user()->role->name == 'Employer')
                                    <li class="layoutli"><a href="{{ url('/jobpost') }}">View Job Posts</a></li>
                                    <li class="layoutli"><a href="{{ url('/jobpost/create') }}">Create Job Post</a></li>
                                @endif
                            </ul>
                        </li>
                        @if (Auth::guest())
                            <li class="layoutli"><a href="{{ url('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a></li>
                            <li class="layoutli"><a href="{{ url('/register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Register</a></li>
                        @else
                            @if(Auth::user()->role->name == 'Admin')
                                <li class="layoutli"><a href="#" class="drop">Users</a>
                                    <ul class="layoutul">
                                        <li class="layoutli"><a href="{{ url('/admin/manageusers') }}">Manage Users</a></li>
                                        <li class="layoutli"><a href="{{ url('/admin/manageemployers') }}">Manage Employers</a></li>
                                        <li class="layoutli"><a href="{{ url('/admin/managejobseekers') }}">Manage Job Seekers</a></li>
                                    </ul>
                                </li>
                                <li class="layoutli"><a href="{{ route('admindashboard') }}"><i class="fa fa-gears" aria-hidden="true"></i> Admin Dashboard</a></li>
                            @endif

                            @if(Auth::user()->role->name == 'Job Seeker')
                                <li class="layoutli"><a href="{{ route('profile.index', Auth::user()->slug) }}">View Profile</a></li>
                            @endif

                            @if(Auth::user()->role->name == 'Employer')
                                <li class="layoutli"><a href="{{ route('profile.index', Auth::user()->slug) }}">View Profile</a></li>
                            @endif

                        <!-- <%
                $roleId = Auth::user()->role;
                $roleName = User::where('role', '=' $roleId)->get();
              %> -->
                            <li class="layoutli">
                                <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout <i class="fa fa-fw fa-power-off" aria-hidden="true"></i>
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif
                    </ul>
                </nav>
                <!-- ################################################################################################ -->
            </header>
        </div>
    </nav>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div id="app">
        @yield('body')
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="layoutwrapper row4">
        <footer id="footer" class="layouthoc layoutclear">
            <!-- ################################################################################################ -->
            <div class="row">
                <div class="col-md-6">
                    <div>
                        <h6 id="lh6" class="title">Social Media</h6>
                        <p>Egestas purus quis arcu sagittis vitae finibus lorem egestas fusce maximus aliquam mi ac placerat tortor molestie.</p>
                        <p class="btmspace-15">Eu metus et elementum luctus nunc in dictum massa in felis feugiat rhoncus proin faucibus ante sit amet metus.</p>
                        <ul class="faico layoutclear">
                            <li class="layoutli"><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="layoutli"><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <!-- <li class="layoutli"><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li> -->
                            <li class="layoutli"><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li class="layoutli"><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <!-- <li class="layoutli"><a class="faicon-vk" href="#"><i class="fa fa-vk"></i></a></li> -->
                        </ul>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="footercol" id="footdiv2">
                        <h6 id="lh6" class="title">Contact</h6>
                        <ul class="layoutnospace linklist  layoutul">
                            <li class="layoutli"><i style="color:#A3D044" class="fa fa-1x fa-map-marker"></i>  Street Name &amp; Number, Town, Postcode/Zip</li>
                            <li class="layoutli"><i style="color:#A3D044" class="fa fa-1x fa-phone"></i>  +00 (123) 456 7890</li>
                            <li class="layoutli"><i style="color:#A3D044" class="fa fa-1x fa-fax"></i>  +00 (123) 456 7890</li>
                            <li class="layoutli"><i style="color:#A3D044" class="fa fa-1x fa-envelope-o"></i>  info@careermanager.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- ################################################################################################ -->
        </footer>
    </div>

    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="layoutwrapper row5">
        <div id="copyright" class="layouthoc layoutclear">
            <!-- ################################################################################################ -->
            <p class="fl_left">Copyright &copy; 2016 - All Rights Reserved - <a href="#">Domain Name</a></p>
            <!-- ################################################################################################ -->
        </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>

    {{--jquery, bootstrap, sweetalert, backtotop, mobilemenu, hideandshownavbar--}}
    <script src="{{ asset('build/js/app.js') }}"></script>
    <script src="{{ asset('build/js/frontendtheme.js') }}"></script>
    @yield('pagescripts')
</div>
</body>
</html>
