<div class="container" style="padding: 220px 0px 0px 0px; color: black" >
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <p class=" text-success">Profile Created : {{ $profile->created_at }}</p>
        </div>
        <div class="col-xs-10 col-sm-12 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $profile->user->name }}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 avatar">
                            <div class="container img-container">
                                <img src="{{ asset($profile->user->avatar) }}" alt="" class=""/>
                                <p class="img-title">Click to Change</p>
                                <div class="overlay"></div>
                                <div class="button"><a data-toggle="modal" data-target="#myModal"> Upload </a>
                                </div>
                            </div>
                            <br>
                            <div class=" col-md-12 col-lg-12">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Company Name:</td>
                                        <td>{{ $profile->company_name == null ? 'not updated yet' : $profile->company_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Industry:</td>
                                        <td>{{ $profile->industry == null ? 'not updated yet' : $profile->industry }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{{ $profile->address == null ? 'not updated yet' : $profile->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>Country:</td>
                                        <td>{{ $profile->country == null ? 'not updated yet' : $profile->country }}</td>
                                    </tr>
                                    <tr>
                                        <td>Contact Number:</td>
                                        <td>{{ $profile->contact_number == null ? 'not updated yet' : $profile->contact_number }}</td>
                                    </tr>
                                    <tr>
                                        <td>About:</td>
                                        <td>{{ $profile->about == null ? 'not updated yet' : $profile->about }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><a href="mailto:{{ $profile->user->email }}"></a>{{ $profile->user->email  }}</td>
                                    </tr>

                                    </tbody>
                                </table>

                                {{--<a href="#" class="btn btn-success">My Sales Performance</a>--}}
                                {{--<a href="#" class="btn btn-success">Team Sales Performance</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div>

                        </div>
                        <div></div>
                        <span class="pull-right">
                            <a type="button" class="btn btn-sm btn-warning" id="flip"><i class="glyphicon glyphicon-edit"> Edit</i></a>
                            {{--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                        </span>
                        <span class="pull-left">
                            <a type="button" class="btn btn-sm btn-danger" id="app_pass"><i class="glyphicon glyphicon-king"> Create Application Password</i></a>
                            {{--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panel" class="container hidden" style="padding: 2px 0px 0px 0px; color: black" >
        <div class="row">
            <div class="col-xs-10 col-sm-12 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $profile->user->name }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 avatar">
                                <div class=" col-md-12 col-lg-12">
                                    {!! Form::model($profile, array('route' => array('employer_profiles.update', $profile->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'profile_edit_form')) !!}
                                    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Company Name: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="company_name" id="company_name" class="form-control" value="{{ $profile->company_name }}">
                                        </div>
                                        @if ($errors->has('company_name'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('company_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Industry: </label>
                                        <div class="col-sm-12">
                                            <select name="industry" id="industry" class="form-control" value="{{ $profile->industry }}">
                                                @foreach(\App\JobCategory::all() as $job_category)
                                                    <option value="{{ $job_category->name }}">{{ $job_category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->has('location'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('location') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Country: </label>
                                        <div class="col-sm-12">
                                            {{ Form::select('country', $countries->all(), $profile->country, array('class' => 'form-control')) }}
                                        </div>
                                        @if ($errors->has('country'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Address: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="address" id="address" class="form-control" value="{{ $profile->address }}">
                                        </div>
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Contact Number: </label>
                                        <div class="col-sm-12">
                                            <input type="number" name="contact_number" id="contact_number" class="form-control" value="{{ $profile->contact_number }}">
                                        </div>
                                        @if ($errors->has('contact_number'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('contact_number') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('about') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">About: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="about" id="about" class="form-control" value="{{ $profile->about }}">
                                        </div>
                                        @if ($errors->has('about'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('about') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block"> Save Changes</button>
                                    </div>
                                    {!! Form::hidden('id', $profile->id) !!}
                                    {{ csrf_field() }}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panel-app_pass" class="container hidden" style="padding: 2px 0px 0px 0px; color: black" >
        <div class="row">
            <div class="col-xs-10 col-sm-12 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $profile->user->name }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 avatar">
                                <div class=" col-md-12 col-lg-12">
                                    {!! Form::model($profile, array('route' => array('employer_profiles.update', $profile->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'profile_edit_form')) !!}
                                    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Company Name: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="company_name" id="company_name" class="form-control" value="{{ $profile->company_name }}">
                                        </div>
                                        @if ($errors->has('company_name'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('company_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    {!! Form::hidden('id', $profile->id) !!}
                                    {{ csrf_field() }}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->