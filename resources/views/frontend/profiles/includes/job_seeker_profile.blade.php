<div class="container" style="padding: 220px 0px 0px 0px; color: black" >
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <p class=" text-success">Profile Created : {{ $profile->created_at }}</p>
        </div>
        <div class="col-xs-10 col-sm-12 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $profile->first_name }} {{ $profile->last_name }}</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="container img-container">
                                {{ Html::image($profile->user->avatar) }}
                                {{--<img src="{{ $profile->user->avatar }}" alt="" class=""/>--}}
                            </div>
                            @if(Auth::user()->email == $profile->user->email)
                                <div class="container" style="margin-top: 20px">
                                    <div class="col-md-6">
                                        <form enctype="multipart/form-data" action="{{ route('profile.avatar') }}" method="POST" class="form-horizontal">
                                            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                                                <label class="col-sm-5" for="avatar">Update Profile Picture</label>
                                                <input class="col-sm-3" type="file" name="avatar" id="avatar">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input class="col-sm-2 col-sm-offset-1 btn btn-xs btn-danger" type="submit" value="Update">
                                            </div>
                                            <div>
                                                @if ($errors->has('avatar'))
                                                    <span class="help-block text-danger">
                                                        <strong>{{ $errors->first('avatar') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                            <br>
                            <div class=" col-md-12 col-lg-12">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <td>Initials:</td>
                                        <td>{{ $profile->initials == null ? 'not updated yet' : $profile->initials }}</td>
                                    </tr>
                                    <tr>
                                        <td>First Name:</td>
                                        <td>{{ $profile->first_name == null ? 'not updated yet' : $profile->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Last Name:</td>
                                        <td>{{ $profile->last_name == null ? 'name not updated yet' : $profile->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Gender:</td>
                                        <td>{{ $profile->gender == null ? 'name not updated yet' : $profile->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td>Location:</td>
                                        <td>{{ $profile->location == null ? 'name not updated yet' : $profile->location }}</td>
                                    </tr>
                                    <tr>
                                        <td>Contact Number:</td>
                                        <td>{{ $profile->mobile == null ? 'name not updated yet' : $profile->mobile }}</td>
                                    </tr>
                                    <tr>
                                        <td>Experience:</td>
                                        <td>{{ $profile->experience == null ? 'name not updated yet' : $profile->experience }}</td>
                                    </tr>
                                    <tr>
                                        <td>Industry:</td>
                                        <td>{{ $profile->industry == null ? 'name not updated yet' : $profile->industry }}</td>
                                    </tr>
                                    <tr>
                                        <td>Qualification:</td>
                                        <td>{{ $profile->qualification == null ? 'name not updated yet' : $profile->qualification }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><a href="mailto:{{ $profile->user->email }}"></a>{{ $profile->user->email  }}</td>
                                    </tr>

                                    </tbody>
                                </table>

                                {{--<a href="#" class="btn btn-success">My Sales Performance</a>--}}
                                {{--<a href="#" class="btn btn-success">Team Sales Performance</a>--}}
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div>

                        </div>
                        <div></div>
                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <a type="button" class="btn btn-sm btn-danger" id="cv" href="{{ route('job_seeker_profiles.resume', [$profile->id]) }}"><i class="fa fa-archive"></i> Resume</a>
                        @if(Auth::user()->email == $profile->user->email)
                            <span class="pull-right">
                                <a type="button" class="btn btn-sm btn-warning" id="flip"><i class="glyphicon glyphicon-edit"> Edit</i></a>
                                {{--<a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>--}}
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="panel" class="container hidden" style="padding: 2px 0px 0px 0px; color: black" >
        <div class="row">
            <div class="col-xs-10 col-sm-12 col-md-6 col-lg-6 col-xs-offset-1 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ $profile->first_name }} {{ $profile->last_name }} : Profile Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 avatar">
                                <div class=" col-md-12 col-lg-12">
                                    {!! Form::model($profile, array('route' => array('job_seeker_profiles.update', $profile->id), 'files' => true, 'method' => 'PUT', 'class' => 'form-horizontal', 'id' => 'profile_edit_form')) !!}
                                    <div class="form-group{{ $errors->has('initials') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">initials: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="initials" id="initials" class="form-control" value="{{ $profile->initials }}">
                                        </div>
                                        @if ($errors->has('initials'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('initials') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">First Name: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $profile->first_name }}">
                                        </div>
                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Last Name: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $profile->last_name }}">
                                        </div>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Gender: </label>
                                        <div class="col-sm-12">
                                            <select name="gender" id="gender" class="form-control" value="{{ $profile->gender }}">
                                                <option value="male">male</option>
                                                <option value="female">female</option>
                                            </select>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('gender') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Location: </label>
                                        <div class="col-sm-12">
                                            {{ Form::select('location', $countries->all(), $profile->location, array('class' => 'form-control')) }}
                                        </div>
                                        @if ($errors->has('location'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('location') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('contact_number') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Contact Number: </label>
                                        <div class="col-sm-12">
                                            <input type="number" name="contact_number" id="contact_number" class="form-control" value="{{ $profile->mobile }}">
                                        </div>
                                        @if ($errors->has('contact_number'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('contact_number') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Experience: <span style="font-size: 10px;">( In Years )</span> </label>
                                        <div class="col-sm-12">
                                            <input type="number" name="experience" id="experience" class="form-control" value="{{ $profile->experience }}">
                                        </div>
                                        @if ($errors->has('experience'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('experience') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('industry') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Industry: </label>
                                        <div class="col-sm-12">
                                            <select name="industry" id="industry" class="form-control" value="{{ $profile->industry }}">
                                                @foreach(\App\JobCategory::all() as $job_category)
                                                    <option value="{{ $job_category->name }}">{{ $job_category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->has('location'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('location') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('qualification') ? ' has-error' : '' }}">
                                        <label class="col-sm-12" for="title">Qualification: </label>
                                        <div class="col-sm-12">
                                            <input type="text" name="qualification" id="qualification" class="form-control" value="{{ $profile->qualification }}">
                                        </div>
                                        @if ($errors->has('qualification'))
                                            <span class="help-block">
                                                        <strong>{{ $errors->first('qualification') }}</strong>
                                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block"> Save Changes</button>
                                    </div>
                                    {!! Form::hidden('id', $profile->id) !!}
                                    {{ csrf_field() }}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





