<style>
    .nav-tabs {
        float: left;
        border-bottom: 0;
    }

    .nav-tabs li {
        float: none;
        margin: 0;
    }

    .nav-tabs li a {
        margin-right: 0;
        border: 0;
        background-color: #c0392b;
    }

    .nav-tabs li a:hover { background-color: #e74c3c; }

    .nav-tabs .glyphicon { color: #fff; }

    .nav-tabs .active .glyphicon { color: #333; }

    .nav-tabs > li.active > a,
    .nav-tabs > li.active > a:hover,
    .nav-tabs > li.active > a:focus { border: 0; }

    .tab-content { margin-left: 45px; }

    .tab-content .tab-pane {
        display: none;
        background-color: #fff;
        padding: 1.6rem;
        overflow-y: auto;
    }

    .tab-content .active { display: block; }

</style>


<ul class="nav nav-tabs">
    <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span></a></li>
    <li><a href="#profile" data-toggle="tab"><span class="glyphicon glyphicon-hourglass"></span></a></li>
    <li><a href="#favourite" data-toggle="tab"><span class="glyphicon glyphicon-duplicate"></span></a></li>
    <li><a href="#download" data-toggle="tab"><span class="glyphicon glyphicon-education"></span></a></li>
    <li><a href="#stars" data-toggle="tab"><span class="glyphicon glyphicon-heart"></span></a></li>
    <li><a href="#settings" data-toggle="tab"><span class="glyphicon glyphicon-link"></span></a></li>
</ul>

<div id="myTabContent" class="tab-content">
    <div class="tab-pane active" id="home">
        <form class="form-horizontal">
            <div class="form-group">
                <h3>Basic Information</h3>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="first_name">First Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="first_name" placeholder="Enter First Name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="last_name">Last Name:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="address">Permanent Address:</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" placeholder="Enter Permanent Address">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="tab-pane" id="profile">
        <div class="input-group control-group after-add-more">
            <input type="text" name="addmore[]" class="form-control" placeholder="Enter Name Here">
            <div class="input-group-btn">
                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
            </div>
        </div>

        <!-- Copy Fields -->
        <div class="copy hide">
            <div class="control-group input-group" style="margin-top:10px">
                <input type="text" name="addmore[]" class="form-control" placeholder="Enter Name Here">
                <div class="input-group-btn">
                    <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="favourite">
        <h3>Qualification</h3>

        <p>Suspendisse blandit libero id suscipit consectetur. Cras ultrices sem sit amet est efficitur iaculis ut non est. Morbi porttitor eu felis ac aliquet.</p>
    </div>
    <div class="tab-pane" id="download">
        <h3>Education</h3>
        <p>Vestibulum viverra ex ut arcu lobortis tempor. Vivamus suscipit dictum nisl gravida malesuada. Nulla et quam eget massa cursus sodales a quis augue. Maecenas ultrices vitae felis ut faucibus. Vivamus vitae convallis nunc, quis vestibulum eros. Morbi vel purus nec justo efficitur consequat ac sit amet risus. Duis laoreet hendrerit bibendum. Nam tempor volutpat sagittis.</p>
    </div>
    <div class="tab-pane" id="stars">
        <h3>Interests</h3>
        <p>Duis laoreet hendrerit bibendum. Nam tempor volutpat sagittis. Etiam convallis posuere rhoncus. Donec sit amet faucibus magna, vel fermentum velit. Vivamus euismod cursus sollicitudin. Aliquam tortor libero, venenatis id nibh consectetur, pretium tincidunt est. Nullam neque odio, auctor in nisl vel, facilisis convallis massa. </p>
    </div>
    <div class="tab-pane" id="settings">
        <h3>References</h3>
        <p>Curabitur vel urna vestibulum, egestas sapien accumsan, varius ipsum. Aenean eu elit laoreet, sagittis velit in, facilisis ligula. Maecenas dictum, lacus sed faucibus lobortis, turpis ante ultricies ligula, et posuere nisl mauris non erat.</p>
    </div>
</div>

<script>
    var tabsFn = (function() {

        function init() {
            setHeight();
        }

        function setHeight() {
            var $tabPane = $('.tab-pane'),
                tabsHeight = $('.nav-tabs').height();

            $tabPane.css({
                height: tabsHeight
            });
        }

        $(init);
    })();

</script>