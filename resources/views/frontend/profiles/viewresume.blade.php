@extends('frontend.layout')

@section('pagestyles')
    <style>
        h3 {
            color: darkred;
        }
        .list-group-item-heading {
            padding-left: 30px;
            font-size: 16px;
        }
        .list-group {
            font-family: Arial;
        }
        .list-group-item-text {
            padding-top: 2px;
            padding-bottom: 2px;
            padding-left: 50px;
            color: darkred;
            font-family: Arial;
        }
        hr{
            color: darkred;
        }
    </style>
@endsection

@section('body')
    <div class="container-fluid" style="padding: 220px 10px 20px 30px; color: black" >
        <div class="row">
            <h3>Resume</h3>
            @if(Auth::user()->role_id == 2)
                @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                    <a class="btn btn-primary"
                       href="/job_seeker_profiles/{{$resume->job_seeker_profile_id}}/resume/create">Create Resume</a>
                @endif
            @endif
        </div>
        <hr>
        <div class="well">
            <div class="list-group">
                <h3>Summary: </h3>
                <hr>
                <h4 class="list-group-item-heading">{{ $resume->summary }}</h4>
            </div>
            <div class="list-group">
                <h3>Skills: </h3>
                <hr>
                @if($resume['skills'])
                    @foreach($resume['skills'] as $skill)
                        <h4 class="list-group-item-heading">{{ $skill['name'] }}</h4>
                    @endforeach
                @endif
            </div>
            <div class="list-group">
                <h3>Experience: </h3>
                <hr>
                @if($resume['experiences'])
                    @foreach($resume['experiences'] as $experience)
                        <h4 class="list-group-item-heading">{{ $experience['name'] }}</h4>
                        <p class="list-group-item-text">Start Date: {{ $experience['start_date'] }}</p>
                        <p class="list-group-item-text">End Date: {{ $experience['end_date'] }}</p>
                    @endforeach
                @endif
            </div>
            <div class="list-group">
                <h3>Education: </h3>
                <hr>
                @if($resume['education'])
                    @foreach($resume['education'] as $education)
                        <h4 class="list-group-item-heading">Education Level: {{ $education['name'] }}</h4>
                        <p class="list-group-item-text">Field: {{ $education['field'] }}</p>
                        <p class="list-group-item-text">University or School: {{ $education['school'] }}</p>
                        <p class="list-group-item-text">Start Date: {{ $education['start_date'] }}</p>
                        <p class="list-group-item-text">End Date: {{ $education['end_date'] }}</p>
                    @endforeach
                @endif
            </div>
            <div class="list-group">
                <h3>Rerefences: </h3>
                <hr>
                @if($resume['references'])
                    @foreach($resume['references'] as $reference)
                        <h4 class="list-group-item-heading">{{ $reference['name'] }}</h4>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@stop

@section('pagescripts')
    @include('flash')
@endsection