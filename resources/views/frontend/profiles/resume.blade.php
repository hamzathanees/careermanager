@extends('frontend.layout')

@section('pagestyles')
    <style>
        li > p {
            padding-right: 5px;
        }
        .remove
        {
            float: right;
        }
        .list-group-item{

            background-color: #F5F5F5;
        }
        .removeMulti
        {
            position:absolute;
            top:25%;
            right:2%;
        }
        .oldresume
        {
            color: darkred;
        }
    </style>
@endsection

@section('body')
    <div class="container-fluid" style="padding: 220px 10px 20px 30px; color: black" >
        @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
        <div class="row">
            <h3>Create or Update Your Resume!</h3>
        </div>
        @endif

        <div class="row" id="app-6">
            @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Update Your Resume Here</div>
                    <div class="panel-body">
                        <div class="form-horizontal" style="margin: 0 10px">
                            <div class="form-group" :class="{ 'control': true }">
                                <label for="summary" >Summary:</label>
                                <textarea name="summary" id="summary" v-model="newSum"  class="form-control" placeholder="Add Summary"></textarea>
                                <button class="btn btn-xs btn-success" v-on:click="addNewSum" style="margin-top: 6px;">Add To Summary</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="skills" >Skills:</label>
                                <select class="form-control" v-model="newSkill">
                                    @foreach(\App\Skill::all() as $skill)
                                        <option style="margin-top: 6px">{{ $skill->name }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" id="experience" class="form-control" v-model="newSkill" placeholder="Add to Skill"/>--}}
                                <button class="btn btn-xs btn-success" v-on:click="addNew" style="margin-top: 6px;">Add To Skill List</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="experience" >Experiences:</label>
                                <textarea rows="5" cols="10" id="experience" class="form-control" v-model="newExp" placeholder="Add Experience"></textarea>
                                {{--<div class='input-group date' id='datetimepicker1'>--}}
                                {{--<input type='text' class="form-control"  v-model="newExpDate" />--}}
                                {{--<span class="input-group-addon">--}}
                                {{--<span class="glyphicon glyphicon-calendar"></span>--}}
                                {{--</span>--}}
                                {{--</div>--}}
                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <input type='date' class="form-control" v-model="newExpSDate" placeholder="Started Date"/>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <input type='date' class="form-control" v-model="newExpEDate" placeholder="End Date"/>
                                    </div>
                                </div>
                                <button class="btn btn-xs btn-success" v-on:click="addNewExp" style="margin-top: 6px;">Add To Experince List</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="education" >Education:</label>
                                <select class="form-control" v-model="newEdu">
                                    @foreach(\App\EducationLevel::all() as $education)
                                        <option style="margin-top: 6px">{{ $education->name }}</option>
                                    @endforeach
                                </select>
                                <label for="education" >Field:</label>
                                <select class="form-control" v-model="newEduField">
                                    @foreach(\App\EducationCategory::all() as $category)
                                        <option style="margin-top: 6px">{{ $category->name }}</option>
                                    @endforeach
                                </select>

                                <label for="education" >University or School Attended:</label>
                                <input type='text' class="form-control" v-model="newEduSchool" placeholder="Attended at"/>

                                <label for="education" >Start and End Date:</label>
                                <br>
                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <input type='date' class="form-control" v-model="newEduSDate" placeholder="Started Date"/>
                                    </div>
                                </div>
                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <input type='date' class="form-control" v-model="newEduEDate" placeholder="End Date"/>
                                    </div>
                                </div>
                                <button class="btn btn-xs btn-success" v-on:click="addNewEdu" style="margin-top: 6px;">Add To Education List</button>
                            </div>

                            <div class="form-group" id="profile">
                                <label for="references" >Rerefences:</label>
                                <textarea rows="5" cols="10" id="references" class="form-control" v-model="newRef" placeholder="Add Reference"></textarea>
                                <button class="btn btn-xs btn-success" v-on:click="addNewRef" style="margin-top: 6px;">Add To Reference List</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $profile->first_name }} {{ $profile->last_name }} ' Resume</div>
                    <div class="panel-body">
                        <div class="form-group row">
                            <label for="first_name_out" class="col-md-3">First Name: </label>
                            <p id="first_name_out" class="col-md-9">{{ $profile->first_name }}</p>
                        </div>
                        <div class="form-group row">
                            <label for="last_name_out" class="col-md-3">Last Name: </label>
                            <p id="last_name_out" class="col-md-9">{{ $profile->last_name }}</p>
                        </div>
                        <div class="form-group row">
                            <label for="summary-out" class="col-md-3">Summary: </label>
                            <p id="summary-out" class="col-md-12">@{{ resume_data.summary }}</p>
                        </div>
                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Skills: </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="skill-item"
                                    v-for="(skill, index) in resume_data.skills"
                                    v-bind:title="skill.name"
                                    v-on:remove="resume_data.skills.splice(index, 1)"
                                ></li>
                                {{--append skill id --}}
                            </ul>
                        </div>

                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Experiences: </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="exp-item"
                                    v-for="(exp, index) in resume_data.experiences"
                                    v-bind:title="exp.name"
                                    v-bind:start_date="exp.start_date"
                                    v-bind:end_date="exp.end_date"
                                    v-on:remove="resume_data.experiences.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>

                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Education: </label>

                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="edu-item"
                                    v-for="(edu, index) in resume_data.education"
                                    v-bind:title="edu.name"
                                    v-bind:edu_field="edu.field"
                                    v-bind:edu_school="edu.school"
                                    v-bind:start_date="edu.start_date"
                                    v-bind:end_date="edu.end_date"
                                    v-on:remove="resume_data.education.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>

                        <div class="form-group row">
                            <label for="skills-out" class="col-md-3">Reference: </label>
                            <ul class="col-md-11 col-md-offset-1 list-group">
                                <li class="list-group-item" is="ref-item"
                                    v-for="(ref, index) in resume_data.references"
                                    v-bind:title="ref.name"
                                    v-on:remove="resume_data.references.splice(index, 1)"
                                ></li>
                            </ul>
                        </div>

                        <div>
                            <input type="hidden" id="token" name="_token" value="{{{ csrf_token() }}}" />
                            @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                                <button class="btn btn-sm btn-danger" style="float: right" v-on:click="saveResume">Save</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<pre>@{{ $data | json }}</pre>--}}

            {{--</div>--}}
        </div>
    </div>
@endsection

@section('pagescripts')
    <script src="{{ asset('build/js/vue.min.js') }}"></script>
    <script src="{{ asset('build/js/vue-resource.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('build/js/datetimepicker.min.js')}}"></script>
    <script>
        // get csrf token
        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

        Vue.component('skill-item', {
            template: '\
                <li>\
                  <p>@{{ title }}</p>\
                  @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                  @endif
                </li>\
                ',
            props: ['title']
        });

        Vue.component('exp-item', {
            template: '\
                <li style="margin-bottom: 4px">\
                  <p>@{{ title }}</p> <p>Started Date:  @{{ start_date }}</p> <p>End Date: @{{ end_date }}</p>\
                  @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                  @endif
                </li>\
                ',
            props: ['title', 'start_date', 'end_date']
        });

        Vue.component('edu-item', {
            template: '\
                <li>\
                  <p>@{{ title }}</p> <p>@{{ edu_field }}</p> <p>@{{ edu_school }}</p> <p>Started Date:  @{{ start_date }}</p> <p>End Date: @{{ end_date }}</p>\
                  @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                  @endif
                </li>\
                ',
            props: ['title', 'edu_field', 'edu_school', 'start_date', 'end_date']
        });

        Vue.component('ref-item', {
            template: '\
                <li>\
                  <p>@{{ title }}</p>\
                  @if(Auth::user()->job_seeker_profile->id == $resume->job_seeker_profile_id)
                  <button class="removeMulti btn btn-xs btn-danger" v-on:click="$emit(\'remove\')">X</button>\
                  @endif
                </li>\
                ',
            props: ['title']
        });

        //        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
        var csrf_token = $('meta[name="csrf-token"]').attr('content');


        new Vue({
            el: '#app-6',
            data: {
                resume_data: {
                    job_seeker_profile_id: {!! $profile->id !!},
                    summary: '',
                    skills: [ ],
                    experiences: [ ],
                    education: [ ],
                    references: [ ]
                },
                job_seeker_profile_id: {!! $profile->id !!},
                newSkillId:'',
                newSkill: '',
                newEdu: '',
                newEduField: '',
                newEduSchool: '',
                newExp: '',
                newExpSDate: '',
                newExpEDate: '',
                newEduSDate: '',
                newEduEDate: '',
                newRef: '',
                newSum: '',
                summary: ''
//                skills: [],
//                experiences: [],
//                education: [],
//                references: []

            },
            mounted: function() {
                this.fetchResume();
            },

            methods: {
                fetchResume: function () {
                    this.$http.get('/job_seeker_profiles/' + this.job_seeker_profile_id + '/resume/data').
                    then(function (response) {
                        this.resume_data.job_seeker_profile_id = this.job_seeker_profile_id;
                        this.resume_data.summary = response.body.summary;
                        {{--this.resume_data.education = {{ $resume['education'] }}--}}
                            if(response.body.education != null){
                                this.resume_data.education = response.body.education;
                            }
                            if(response.body.experiences != null){
                                this.resume_data.experiences = response.body.experiences;
                            }
                            if(response.body.skills != null){
                                this.resume_data.skills = response.body.skills;
                            }
                            if(response.body.references != null){
                                this.resume_data.references = response.body.references;
                            }
                    });
                },
                addNewSum: function () {
                    if (this.newSum != ''){
//                        this.skills.push(this.newSkill)
                        this.resume_data.summary = this.newSum;
                        this.newSum = ''
                    }
                },
                addNew: function () {
                    if (this.newSkill != '') {
                        this.resume_data.skills.push({name: this.newSkill});
                        this.newSkill = ''
                        this.newSkillId = ''
                    }
                },
                addNewExp: function () {
                    if (this.newExp != '' && this.newExpDate != ''){
//                        Object.assign(this.experiences, this.newExp, this.newExpDate)
//                        this.experiences.push({name: this.newExp , date: this.newExpDate});
                        this.resume_data.experiences.push({name: this.newExp , start_date: this.newExpSDate, end_date: this.newExpEDate});
                        this.newExp = ''
                        this.newExpSDate = ''
                        this.newExpEDate = ''
                    }
                },

                addNewEdu: function () {
                    if (this.newEdu != ''){
//                        this.education.push(this.newEdu)
                        this.resume_data.education.push({name: this.newEdu , field: this.newEduField, school: this.newEduSchool, start_date: this.newEduSDate, end_date: this.newEduEDate});
                        this.newEdu = ''
                        this.newEduField = ''
                        this.newEduSchool = ''
                        this.newEduSDate = ''
                        this.newEduEDate = ''
                    }
                },

                addNewRef: function () {
                    if (this.newRef != ''){
//                        this.references.push(this.newRef)
                        this.resume_data.references.push({name: this.newRef})
                        this.newRef = ''
                    }
                },

                saveResume: function () {
                    this.$http.post('/job_seeker_profiles/resume', this.resume_data)
                        .then(response => {
                            this.newSum = ''
                            this.newSkill = ''
                            this.newExp = ''
                            this.newExpSDate = ''
                            this.newExpEDate = ''
                            this.newEdu = ''
                            this.newEduSDate = ''
                            this.newEduSDate = ''
                            swal(
                                'Success!!',
                                'Success! Resume successfully Updated!',
                                'success'
                            );
                            console.log(response.resume_data)
                        }).catch(response => {
                        let errors = response.body;
                        console.log(errors);
                    })
                }


            }
        });

    </script>

    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$('#datetimepicker6').datetimepicker({--}}
                {{--format : "YYYY-MM-DD HH:mm"--}}
            {{--});--}}
            {{--$('#datetimepicker7').datetimepicker({--}}
                {{--useCurrent: false, //Important! See issue #1075--}}
                {{--format : "YYYY-MM-DD HH:mm"--}}
            {{--});--}}
            {{--$("#datetimepicker6").on("dp.change", function (e) {--}}
                {{--$('#datetimepicker7').data("DateTimePicker").minDate(e.date);--}}
            {{--});--}}
            {{--$("#datetimepicker7").on("dp.change", function (e) {--}}
                {{--$('#datetimepicker6').data("DateTimePicker").maxDate(e.date);--}}
            {{--});--}}

            {{--$('#datetimepicker8').datetimepicker({--}}
                {{--format : "YYYY-MM-DD HH:mm"--}}
            {{--});--}}
            {{--$('#datetimepicker9').datetimepicker({--}}
                {{--useCurrent: false, //Important! See issue #1075--}}
                {{--format : "YYYY-MM-DD HH:mm"--}}
            {{--});--}}
            {{--$("#datetimepicker8").on("dp.change", function (e) {--}}
                {{--$('#datetimepicker9').data("DateTimePicker").minDate(e.date);--}}
            {{--});--}}
            {{--$("#datetimepicker9").on("dp.change", function (e) {--}}
                {{--$('#datetimepicker8').data("DateTimePicker").maxDate(e.date);--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

    {{--@include('flash')--}}

@endsection