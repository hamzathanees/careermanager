@inject('countries', 'App\Http\Utilities\Country')
@extends('frontend.layout')

@section('pagestyles')
    <style>
        .media-object{
            width: 64px;
            height: 64px;
        }

        #replyform{
            display: none;
        }
        .img-container {
            position: relative;
            margin-top: 10px;
            width: 250px;
            height: 250px;
        }

        img {
            position: absolute;
            width: 250px;
            height: 250px;
            left: 0;
        }

    </style>
@endsection

@section('body')

    @if($profile->user->role->name == 'Job Seeker')
        @include('frontend.profiles.includes.job_seeker_profile')
    @endif
    @if($profile->user->role->name == 'Employer')
        @include('frontend.profiles.includes.employer_profile')
    @endif


@stop

@section('pagescripts')
    <script>
        $(document).ready(function(){
            $("#flip").click(function(){
                $("#panel").toggle().removeClass('hidden').addClass('animated slideInDown');
            });

            $("#cv").click(function(){
                $("#panel-cv").toggle().removeClass('hidden').addClass('animated slideInDown');
            });

            $("#app_pass").click(function(){
                $("#panel-app_pass").toggle().removeClass('hidden').addClass('animated slideInDown');
            });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function() {

            $(".add-more").click(function(){
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });

            $("body").on("click",".remove",function(){
                $(this).parents(".control-group").remove();
            });

        });

    </script>
    @include('flash')
@endsection