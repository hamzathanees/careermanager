@extends('frontend.layout')

@section('pagestyles')
    <style>
        .media-object{
            width: 64px;
            height: 64px;
        }

        #replyform{
            display: none;
        }

    </style>
@endsection

@section('body')

    <div class="container" style="padding: 160px 0px 0px 0px; color: black" >
        <p style="color: #ff001c;">Notice: Experience points and Educational points below 30 are less than Employer requirement</p>
        <div class="row">
            <div class="col-md-12">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Profile</th>
                        <th>Job Seeker Email</th>
                        <th>Education</th>
                        <th>Skills</th>
                        <th>Experience</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ranks as $rank)
                        <tr>
                            <?php
                                 $job_seeker = \App\JobSeekerProfile::where('id', '=', $rank->job_seeker_profile_id)->first();
                            ?>
                            <td><a href="{{ route('profile.index', $job_seeker->user->slug) }}">{{ $job_seeker->first_name == '' ? 'first name' : $job_seeker->first_name }} {{ $job_seeker->last_name == '' ? 'last name' : $job_seeker->last_name }}</a></td>
                            <td>{{ $job_seeker->user->email }}</td>
                            <td class="edu">{{ $rank->education_point == null ? 0 : round($rank->education_point, 2)}}</td>
                            <td class="skill">{{ $rank->skill_point == null ? 0 : round($rank->skill_point, 2)}}</td>
                            <td class="exp">{{ $rank->experience_point == null ? 0 : round($rank->experience_point, 2)}}</td>

                            <?php
                                    $edu = $rank->education_point == null ? 0 : round($rank->education_point, 2);
                                    $skill = $rank->skill_point == null ? 0 : round($rank->skill_point, 2);
                                    $exp = $rank->experience_point == null ? 0 : round($rank->experience_point, 2);

                                    $total = round((($edu + $skill + $exp)), 2);
                            ?>

                            <td>{{ $total }}</td>
                            {{--<td>{{substr($jobpost->title, 0, 20)}} {{ strlen($jobpost->title) > 20 ? "..." : "" }}</td>--}}
                            {{--<td>{{ substr(strip_tags($jobpost->description), 0, 30)}} {{ strlen(strip_tags($jobpost->description)) > 30 ? "..." : "" }}</td>--}}
                            {{--<td>{{ substr(date('M j, Y h:ia', strtotime($jobpost->due_date)), 0, 50)}} {{ strlen(date('M j, Y h:ia', strtotime($jobpost->due_date))) > 50 ? "..." : "" }}</td>--}}
                            {{--<td>{{ $jobpost->comments()->count() }}</td>--}}
                            {{--@if(Auth::user()->role->name == 'Admin')--}}
                                {{--<td>--}}
                                    {{--<a class="btn btn-primary btn-xs" href="{{ route('jobpost.show', $jobpost->id) }}"><i class="fa fa-eye"></i> View</a>--}}
                                    {{--<a class="btn btn-warning btn-xs" href="{{ route('jobpost.edit', $jobpost->id) }}"><i class="fa fa-pencil"></i> Edit</a>--}}
                                    {{--{!! Form::open(['method' => 'DELETE', 'route' => ['frontendjobpost.destroy', $frontendjobpost->id], 'onsubmit' => 'return ConfirmDelete()']) !!}--}}
                                    {{--{!! Form::button('<i class="fa fa-trash"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-xs')) !!}--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</td>--}}
                            {{--@endif--}}
                            {{--@if(Auth::user()->role->name == 'Job Seeker')--}}
                                {{--<td>--}}
                                    {{--<a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$jobpost->slug) }}">{{ url('job_post/'.$jobpost->slug) }}</a>--}}
                                {{--</td>--}}
                            {{--@endif--}}
                            {{--@if(Auth::user()->role->name == 'Employer')--}}
                                {{--<td>--}}
                                    {{--<a class="btn btn-primary btn-xs" href="{{ url('job_post/'.$jobpost->slug) }}">{{ url('job_post/'.$jobpost->slug) }}</a>--}}
                                {{--</td>--}}
                            {{--@endif--}}
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Profile</th>
                        <th>Job Seeker Email</th>
                        <th>Edu</th>
                        <th>Skills</th>
                        <th>Exp</th>
                        <th>Total</th>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>

@stop

@section('pagescripts')
    @include('flash')

    <script>
        $('.exp, .edu').filter(function () {
            return $(this).text() < 30;
        }).css('color', 'red');
        $(function () {
            $("#datatable").DataTable({
                scrollX: true,
                lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
            });
        });
    </script>
@endsection