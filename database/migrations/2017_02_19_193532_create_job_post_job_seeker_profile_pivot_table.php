<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostJobSeekerProfilePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('job_post_job_seeker_profile', function (Blueprint $table) {
			$table->integer('job_post_id')->unsigned()->index();
			$table->foreign('job_post_id')->references('id')->on('job_posts')->onDelete('cascade');
			$table->integer('job_seeker_profile_id')->unsigned()->index();
			$table->foreign('job_seeker_profile_id')->references('id')->on('job_seeker_profiles')->onDelete('cascade');
			$table->primary(['job_post_id', 'job_seeker_profile_id'], 'job_post_apply');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('job_post_job_seeker_profile');
    }
}
