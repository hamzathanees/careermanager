<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobCategoryJobPostPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_category_job_post', function (Blueprint $table) {
            $table->integer('job_category_id')->unsigned()->index();
            $table->foreign('job_category_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->integer('job_post_id')->unsigned()->index();
            $table->foreign('job_post_id')->references('id')->on('job_posts')->onDelete('cascade');
            $table->primary(['job_category_id', 'job_post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_category_job_post');
    }
}
