<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExperiencePointToJobPostJobSeekerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_post_job_seeker_profile', function (Blueprint $table) {
            //
			$table->float('experience_point')->nullable()->after('skill_point');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_post_job_seeker', function (Blueprint $table) {
            //
			$table->dropColumn('experience_point');
        });
    }
}
