<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkillPointToJobPostJobSeekerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_post_job_seeker_profile', function (Blueprint $table) {
            //
			$table->float('skill_point')->nullable()->after('education_point');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_post_job_seeker', function (Blueprint $table) {
            //
			$table->dropColumn('skill_point');
        });
    }
}
