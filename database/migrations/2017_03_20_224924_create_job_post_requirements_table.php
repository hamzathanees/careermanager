<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_post_id')->unsigned()->index();
            $table->foreign('job_post_id')->references('id')->on('job_posts')->onDelete('cascade');
			$table->json('skills')->nullable();
			$table->json('experiences')->nullable();
			$table->json('education')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_requirements');
    }
}
