<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('job_seeker_profile_id')->unique()->unsigned()->index();
			$table->foreign('job_seeker_profile_id')->references('id')->on('job_seeker_profiles')->onDelete('cascade');
			$table->text('summary')->nullable();
			$table->json('skills')->nullable();
			$table->json('experiences')->nullable();
			$table->json('education')->nullable();
			$table->json('references')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
