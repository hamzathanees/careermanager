<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleandUserTableSeeder::class);
        $this->call(JobCategoryTableSeeder::class);
    }
}
