<?php

use Illuminate\Database\Seeder;

class JobCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		
		App\JobCategory::create([
			'name' => 'Airlines/Aviation',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Alternative Dispute Resolution',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Alternative Medicine',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Apparel & Fashion',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Architecture & Planning',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Arts and Crafts',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Automotive',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Aviation & Aerospace',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Banking',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Biotechnology',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Computer & Network Security',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Computer Games',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Computer Hardware',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Computer Networking',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Computer Software',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Web Development',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Software Development',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Network Admnistrator',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'IOS Developer',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Full Stack Developer',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Database Analyst',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Android Developer',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);
		App\JobCategory::create([
			'name' => 'Software Project Manager',
			'details' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ducimus in iusto laboriosam libero molestias? Assumenda deleniti harum molestiae? Consectetur dolorum earum eligendi excepturi inventore, molestiae porro quae ullam vero.',
		]);


//		'Broadcast Media'
//		'Building Materials'
//		'Business Supplies and Equipment'
//		'Capital Markets' 'Chemicals'
//		'Civic & Social Organization'
//		'Civil Engineering'
//		'Commercial Real Estate'
//
//
//
//
//
//		'Construction'
//		'Consumer Electronics'
//		'Consumer Goods'
//		'Consumer Services'
//		'Cosmetics'
//		'Dairy'
//		'Defense & Space'
//		'Design'
//		'Education Management'
//		'E-Learning'
//		'Electrical/Electronic Manufacturing'
//		'Entertainment'
//		'Environmental Services'
//		'Events Services'
//		'Executive Office'
//		'Facilities Services'
//		'Farming'
//		'Financial Services'
//		'Fine Art' 'Fishery'
//		'Food & Beverages'
//		'Food Production'
//		'Fund-Raising'
//		'Furniture'
//		'Gambling & Casinos'
//		'Glass'
//		'Government Administration'
//		'Government Relations'
//		'Graphic Design'
//		'Health'
//		'Higher Education'
//		'Hospital & Health Care'
//		'Hospitality'
//		'Human Resources'
//		'Import and Export'
//		'Individual & Family Services'
//		'Industrial Automation'
//		'Information Services'
//		'Information Technology and Services'
//		'Insurance'
//		'International Affairs'
//		'International Trade and Development'
//		'Internet'
//		'Investment Banking'
//		'Investment Management'
//		'Judiciary'
//		'Law Enforcement'
//		'Law Practice'
//		'Legal Services'
//		'Legislative Office'
//		'Leisure'
//		'Libraries'
//		'Logistics and Supply Chain'
//		'Luxury Goods & Jewelry'
//		'Machinery'
//		'Management Consulting'
//		'Maritime'
//		'Market Research'
//		'Marketing and Advertising'
//		'Mechanical or Industrial Engineering'
//		'Media Production'
//		'Medical Devices'
//		'Medical Practice'
//		'Mental Health Care'
//		'Military'
//		'Mining & Metals'
//		'Motion Pictures and Film'
//		'Museums and Institutions'
//		'Music'
//		'Nanotechnology'
//		'Newspapers'
//		'Non-Profit Organization Management'
//		'Oil & Energy'
//		'Online Media'
//		'Outsourcing/Offshoring'
//		'Package/Freight Delivery'
//		'Packaging and Containers'
//		'Paper & Forest Products'
//		'Performing Arts'
//		'Pharmaceuticals'
//		'Philanthropy'
//		'Photography'
//		'Plastics'
//		'Political Organization'
//		'Primary/Secondary Education'
//		'Printing'
//		'Professional Training & Coaching'
//		'Program Development'
//		'Public Policy'
//		'Public Relations and Communications'
//		'Public Safety'
//		'Publishing'
//		'Railroad Manufacture'
//		'Ranching'
//		'Real Estate'
//		'Recreational Facilities and Services'
//		'Religious Institutions'
//		'Renewables & Environment'
//		'Research'
//		'Restaurants'
//		'Retail'
//		'Security and Investigations'
//		'Semiconductors'
//		'Shipbuilding'
//		'Sporting Goods'
//		'Sports'
//		'Staffing and Recruiting'
//		'Supermarkets'
//		'Telecommunications'
//		'Textiles'
//		'Think Tanks'
//		'Tobacco'
//		'Translation and Localization'
//		'Transportation/Trucking/Railroad'
//		'Utilities'
//		'Venture Capital & Private Equity'
//		'Veterinary'
//		'Warehousing'
//		'Wholesale'
//		'Wine and Spirits'
//		'Wireless'
//		'Writing and Editing'
    }
}
