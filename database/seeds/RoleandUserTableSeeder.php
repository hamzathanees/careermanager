<?php

use Illuminate\Database\Seeder;

class RoleandUserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		App\Role::create([
			'name' => 'Admin',
		]);

		App\Role::create([
			'name' => 'Job Seeker',
		]);

		App\Role::create([
			'name' => 'Employer',
		]);

		App\User::create([
			'name' 		=> 	'admin',
			'email' 	=> 	'admin@cm.com',
			'role_id' 	=> 	1,
			'password' 	=> 	bcrypt('1234'),
			'slug' 		=> 	'admin'
		]);


		App\User::create([
			'name' 		=> 'jobseeker',
			'email' 	=> 'jobseeker@cm.com',
			'role_id' 	=> 	2,
			'password' 	=> 	bcrypt('1234'),
			'slug' 		=> 'jobseeker'
		]);

		App\JobSeekerProfile::create([
			'user_id' 		=> 	2,
			'initials'		=> 'Mr',
			'first_name'	=> 'Job',
			'last_name'		=> 'Seeker',
			'gender'		=> 'male',
			'location'		=> 'Sri Lanka',
			'mobile'		=> 	777353863,
			'experience'	=> '3 years',
			'industry'		=> 'Fake',
			'qualification'	=> 'BSc Hons in Fake',
			'resume'		=> 'public/resumes/jobseeker.json'
		]);

		App\Resume::create([
			'job_seeker_profile_id' => 1,
			'summary' => 'Willing to relocate: Anywhere : sponsorship required to work in the US',
			'skills' => [
				['name' => 'C#'],
				['name' => 'Java'],
				['name' => 'C'],
				['name' => 'C++'],
				['name' => 'HTML 5'],
				['name' => 'SQL'],
				['name' => 'noSQL'],
				['name' => 'mongoDb'],
				['name' => 'Javascript'],
				['name' => 'Oracle']
			],
			'education' => [
				[
					'name' => 'Master of Science in Computer Science : California State University',
					'start_date' => '2015',
					'end_date' => '2017'
				],
				[
					'name' => 'Bachelor of Engineering in Computer Science : Visvesvaraya Technological University',
					'start_date' => 'June 2009',
					'end_date' => 'June 2013'
				]
			],
			'experiences' => [
				[
					'name' => 'Accord Software and Systems, Bangalore India 2 Years 1 Month',
					'start_date' => 'Aug 2013',
					'end_date' => 'Aug 2015'
				]
			],
			'references' => [
				['name' => 'https://www.linkedin.com/in/sai-kiran-13285a70'],
				['name' => 'Email me on Indeed: indeed.com/r/85122db7f79c7784']
			]
		]);

		App\User::create([
			'name' 		=> 'jobseeker 1',
			'email' 	=> 'jobseeker1@cm.com',
			'role_id' 	=> 	2,
			'password' 	=> 	bcrypt('1234'),
			'slug' 		=> 'jobseeker_1'
		]);

		App\JobSeekerProfile::create([
			'user_id' 		=> 	3,
			'initials'		=> 'Mr',
			'first_name'	=> 'Yasith',
			'last_name'		=> 'Lokuge',
			'gender'		=> 'male',
			'location'		=> 'Sri Lanka',
			'mobile'		=> 	0777777,
			'experience'	=> '5 year',
			'industry'		=> 'Information Technology',
			'qualification'	=> 'Bachelors degree in Computer Science - University of Colombo School of Computing',
			'resume'		=> 'public/resumes/jobseeker.json'
		]);

		App\Resume::create([
			'job_seeker_profile_id' => 2,
			'summary' => 'I am an energetic and enthusiastic individual who is driven towards 
			realizing personal goals with self motivation. My passion to work with cutting-edge 
			technology is primarily accountable for my involvement in work related to software 
			engineering, middleware technologies, Internet of Things and embedded systems. I have always 
			enjoyed working in the software industry and find the work both challenging and satisfying. 
			Furthermore, I consider an opportunity to enhance my knowledge and skills 
			as an immense privilege.',
			'skills' => [
				['name' => 'Web Services & Security'],
				['name' => 'RESTful webservice middleware technologies'],
				['name' => 'Jersey Framework Apache Shiro Security framework'],
				['name' => 'WSO2 Microservices framework for Java (Opensource)'],
				['name' => 'Dropwizard'],
				['name' => 'Java EE'],
				['name' => 'Java EE back end development'],
				['name' => 'Internet of Things Architectures Protocols Security'],
				['name' => 'Eclipse Paho Californium Mosquitto MQTT CoAP AMQP'],
				['name' => 'GIT JIRA Scrum Agile Eclipse Maven'],
				['name' => 'Eclipse Paho Californium Mosquitto MQTT CoAP AMQP'],
				['name' => 'Redis'],
				['name' => 'Orient DB'],
				['name' => 'Mysql'],
				['name' => 'Nosql'],
				['name' => 'Graph Databases'],
				['name' => 'JDBI Hibernate'],
				['name' => 'Dev Ops Cloud & Infrastructure'],
				['name' => 'Google Cloud Platform'],
				['name' => 'AWS'],
				['name' => 'Azure'],
				['name' => 'Ansible'],
				['name' => 'Pingdom'],
				['name' => 'Pagerduty'],
				['name' => 'Mailman'],
				['name' => 'WSO2 Product stack']
			],
			'education' => [
				[
					'name' => 'Bachelors degree Field Of Study Computer Science at University of Colombo School of Computing',
					'start_date' => '2011',
					'end_date' => '2014'
				],
				[
					'name' => 'Schooling at Ananda College',
					'start_date' => '2001',
					'end_date' => '2010'
				]
			],
			'experiences' => [
				[
					'name' => 'Software Engineer - Techsurge Innovations (Pvt) Ltd',
					'start_date' => 'November 2010',
					'end_date' => 'June 2014'
				],
				[
					'name' => 'Chairperson at the IEEE Sri Lanka Section Student',
					'start_date' => '2014',
					'end_date' => '2014'
				],
				[
					'name' => 'Research Assistant at adhoc and sensor network',
					'start_date' => 'June 2014',
					'end_date' => 'January 2015'
				],
				[
					'name' => 'Software Engineer at Virtusa Pvt Ltd',
					'start_date' => 'January 2015',
					'end_date' => 'June 2015'
				],
				[
					'name' => 'Software Engineer at WSO2. Telco Pvt Ltd',
					'start_date' => 'January 2016',
					'end_date' => 'Present'
				]
			],
			'references' => [
				['name' => 'http://lk.linkedin.com/in/yasithlokuge'],
				['name' => 'http://yasith.me']
			]
		]);

		App\User::create([
			'name' 		=> 'employer',
			'email' 	=> 'employer@cm.com',
			'role_id' 	=> 3,
			'password' 	=> bcrypt('1234'),
			'slug' 		=> 'employer'
		]);

		App\EmployerProfile::create([
			'user_id' 	=> 	4
		]);


		$this->command->info('Role, Users, EmployerProfile and Job SeekerProfile and Job Seeker Resumes Table Successfully Seeded');
	}
}
