var elixir = require('laravel-elixir');

// require('laravel-elixir-vue-2');
//
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

// elixir((mix) => {
//     mix.sass('app.scss')
//        .webpack('app.js');
// });

elixir(function (mix) {
    mix.sass('app.scss');

    //can be used for all themes  //these scripts and styles are common for both backend and frontend
    mix.styles([
        "libs/bootstrap/dist/css/bootstrap.min.css",
        "libs/datatables/jquery.dataTables.min.css",
        "libs/datetimepicker.min.css",
        "libs/font-awesome/css/font-awesome.min.css",
        "libs/Ionicons/css/ionicons.min.css",
        "libs/sweetalert/dist/sweetalert.css",
        "libs/animate.css/animate.min.css",
        "libs/jvectormap/jquery-jvectormap.css",
        "libs/html5shiv/dist/html5shiv.min.js",
        "libs/respond/dest/respond.min.js"
    ], 'public/build/css/app.css', 'resources/assets');

    mix.scripts([
        "libs/jquery/dist/jquery.min.js",
        "libs/vue/dist/vue.min.js",
        "libs/bootstrap/dist/js/bootstrap.min.js",
        "libs/datatables/jquery.dataTables.min.js",
        "libs/datatables/dataTables.bootstrap.min.js",
        "libs/sweetalert/dist/sweetalert-dev.js",
        "libs/sparkline/jquery.sparkline.min.js",
        "libs/jvectormap/jquery-jvectormap.js",
        "libs/jvectormap/tests/assets/jquery-jvectormap-world-mill-en.js",
        "libs/chart.js/dist/Chart.min.js",
        "libs/chart.js/dist/Chart.min.js"
    ], 'public/build/js/app.js', 'resources/assets');

    mix.copy('resources/assets/libs/font-awesome/fonts', 'public/build/fonts');
    mix.copy('resources/assets/libs/bootstrap/fonts','public/build/fonts');

    //only to be used for the frontend theme
    mix.styles([
        "libs/frontendtheme.css"
    ], 'public/build/css/frontendtheme.css', 'resources/assets');

    mix.scripts([
        "libs/jquery.backtotop.js",
        "libs/jquery.mobilemenu.js",
        "libs/jquery.hideandshownavbar.js"
    ], 'public/build/js/frontendtheme.js', 'resources/assets');


    //only to be used for the backend theme
    mix.styles([
        "libs/AdminLTE.min.css",
        "libs/AdminLTE_all-skins.min.css",
    ], 'public/build/css/backendtheme.css', 'resources/assets');

    mix.scripts([
        "libs/AdminLTE.js"
    ], 'public/build/js/backendtheme.js', 'resources/assets');


    //only for auth theme
    mix.styles([
        "libs/ace.min.css"
    ], 'public/build/css/auththeme.css', 'resources/assets');

});
