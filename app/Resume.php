<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
//	use Translatable;
	/**
	 * @var array
	 */
	protected $fillable = [
		'job_seeker_profile_id', 'summary', 'skills', 'experiences', 'education', 'references'
	];


//	public $translatable = ['skills', 'experiences', 'education', 'references'];

	protected $casts = [
		'skills' => 'json',
		'experiences' => 'json',
		'education' => 'json',
		'references' => 'json'
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function job_seeker()
	{
		return $this->belongsTo('App\JobSeekerProfile', 'job_seeker_profile_id', 'id');
	}

//	public function getSkillsAttribute($value)
//	{
//		return json_decode($value);
//	}

}
