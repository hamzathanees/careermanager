<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobSeekerProfile extends Model
{
    //
	protected $table	=	'job_seeker_profiles';
	protected $fillable	=	['user_id','initials','first_name','last_name','gender','location','mobile','experience',
						 'key_skills','industry','qualification','resume'];

	/**
	 * Get the user that owns the profiles.
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function resume()
	{
		return $this->hasOne('App\Resume', 'id', 'job_seeker_profile_id');
	}


	public function applies()
	{
		return $this->belongsToMany('App\JobPost', 'job_post_job_seeker_profile', 'job_seeker_profile_id', 'job_post_id');
	}

	public function applications()
	{
		return $this->hasMany('App\JobPostJobSeekerProfile');
	}
}
