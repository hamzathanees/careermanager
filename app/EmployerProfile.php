<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployerProfile extends Model
{
    //
	protected $table = 'employer_profiles';
	protected $fillable=['user_id','company_name','industry','address','country','contact_number'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

}
