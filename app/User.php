<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    /**
     * @param $roles
     * @return bool
     */
    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();
        // Check if the user is a root account
        if($this->have_role->name == 'Root') {
            return true;
        }
        if(is_array($roles)){
            foreach($roles as $need_role){
                if($this->checkIfUserHasRole($need_role)) {
                    return true;
                }
            }
        } else{
            return $this->checkIfUserHasRole($roles);
        }
        return false;
    }

    /**
     * @return mixed
     */
    private function getUserRole()
    {
        return $this->role()->getResults();
    }

    /**
     * @param $need_role
     * @return bool
     */
    private function checkIfUserHasRole($need_role)
    {
        return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 * a user with role of job_seeker has one job_seeker profiles
	 */
	public function job_seeker_profile()
	{
		return $this->hasOne('App\JobSeekerProfile');
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 * a user with role of employer has one employer profiles
	 */
	public function employer_profile()
	{
		return $this->hasOne('App\EmployerProfile');
	}


	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 * a user with role admin and employer can create many job posts
	 */
	public function job_posts()
	{
		return $this->hasMany('App\JobPost');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 * any user with any role can create many comments
	 */
	public function job_post_comments()
	{
		return $this->hasMany('App\JobPostComments');
	}

//	public function setPasswordAttribute($password) {
//		$this->attributes['password'] = $password;
//	}

}