<?php

function flash($title = null, $message = null)
{
    $flash = app('App\Http\Flash');

    if (func_num_args() == 0)
    {
        return $flash; //flash()->success() or flash()->info() or flash()->error() or flash()->overlay()
    }

    return $flash->info($title, $message); //flash('Title' 'Body');
}