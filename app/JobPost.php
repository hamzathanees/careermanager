<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    //
	protected $table = 'job_posts';

	protected $fillable = [
		'title', 'description', 'slug', 'due_date', 'user_id'
	];

	public function job_categories()
	{
		return $this->belongsToMany('App\JobCategory');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function comments()
	{
		return $this->hasMany('App\JobPostComment');
	}

	public function job_post_requirement()
	{
		return $this->hasOne('App\JobPostRequirement');
	}

	public function applied_by()
	{
		return $this->belongsToMany('App\JobSeekerProfile', 'job_post_job_seeker_profile', 'job_post_id', 'job_seeker_profile_id');
	}

	public function resume()
	{
		return $this->belongsToThrough('App\JobSeekerProfile', 'App\Resume');
	}

}
