<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostComment extends Model
{
    //
	protected $table = 'job_post_comments';

	protected $fillable = [
		'user_id', 'job_post_id', 'comment', 'approved', 'parent_id'
	];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function job_post()
	{
		return $this->belongsTo('App\JobPost');
	}

	public function replies()
	{
		return $this->hasMany('App\JobPostComment', 'parent_id');
	}
}
