<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostJobSeekerProfile extends Model
{
    //
	protected $table = 'job_post_job_seeker_profile';

	public function job_seeker()
	{
		return $this->belongsTo('App\JobSeekerProfile');
	}

	public function job_post()
	{
		return $this->belongsToMany('App\JobPost');
	}
}
