<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostRequirement extends Model
{
    //
	protected $table = 'job_post_requirements';

	protected $fillable = [
		'job_post_id', 'skills', 'experiences', 'education'
	];

	protected $casts = [
		'skills' => 'array',
		'experiences' => 'array',
		'education' => 'array'
	];

	public function job_post()
	{
		return $this->belongsTo('App\JobPost');
	}
}
