<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    //
    protected $table = 'job_categories';

    protected $fillable = [
        'name', 'details'
    ];

	protected $hidden = array('created_at', 'updated_at', );

	public function job_posts()
	{
		return $this->belongsToMany('App\JobPost');
	}

//	public function getRouteKeyName()
//	{
//		return 'name';
//	}

}
