<?php

namespace App\Http\Controllers;

use App\JobPost;
use App\JobPostRequirement;
use Illuminate\Http\Request;


class JobPostRequirementController extends Controller
{
    //
	public function getPage($id)
	{
		//
		$jobpost = JobPost::find($id);
		return view('jobpost.requirement', compact('jobpost'));
	}

	public function getRequirements($id)
	{
		$job_post_requirements = JobPostRequirement::where('job_post_id', '=', $id)->first();

		return $job_post_requirements;
	}

	public function saveRequirements(Request $request, $id)
	{
		$skills = $request->input('skills');
		$experiences = $request->input('experiences');
		$education = $request->input('education');


		$job_post_requirement = JobPostRequirement::firstOrNew(array('job_post_id' => $id));
		$job_post_requirement->skills = $skills;
		$job_post_requirement->experiences = $experiences;
		$job_post_requirement->education = $education;
		$job_post_requirement->save();

		if ($job_post_requirement->save()){
			flash()->overlay('Success', 'Job Post Requirements Successfully Updated');
			return $job_post_requirement;
		}

	}
}
