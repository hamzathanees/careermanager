<?php

namespace App\Http\Controllers\Api;

use App\EmployerProfile;
use App\JobPost;
use App\JobPostRequirement;
use App\JobSeekerProfile;
use App\Resume;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$jobposts = JobPost::with(['user', 'job_categories'])->get();

		return $jobposts;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$post = JobPost::where('id', '=', $id)->first();
		return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function applications($slug)
	{
		//
//		$job_post = JobPost::find($id);

//		$applicants = Resume::with('');
//		$applicants = DB::table('job_seeker_profiles')
//			->join('job_post_job_seeker_profile', 'job_post_job_seeker_profile.job_seeker_profile_id', '=', 'job_seeker_profiles.id')
//			->join('job_posts', 'job_post_job_seeker_profile.job_post_id', '=', 'job_posts.id')
////			->join('resumes', 'resumes.job_seeker_profile_id', '=', 'job_seeker_profiles.id')
//			->where('job_posts.slug', '=', $slug)
//			->get();

//		$applicants = JobSeekerProfile::with('applies')->get();

		$applicants = DB::table('job_seeker_profiles')
			->join('job_post_job_seeker_profile', 'job_post_job_seeker_profile.job_seeker_profile_id', '=', 'job_seeker_profiles.id')
			->join('job_posts', 'job_post_job_seeker_profile.job_post_id', '=', 'job_posts.id')
			->where('job_posts.slug', '=', $slug)
			->select('job_seeker_profiles.*')
			->get();


		foreach ($applicants as $applicant)
		{
			$resumes[]  = Resume::with('job_seeker')->where('id', '=', $applicant->id)->get();
		}


		return $resumes;


//		echo $arr['data']['current_condition'][0]['temp_F'];

	}

	public function requirementData($slug)
	{
		$requirement_db = DB::table('job_posts')
			->join('job_post_requirements', 'job_post_requirements.job_post_id', '=', 'job_posts.id')
			->where('job_posts.slug', '=', $slug)
			->select('job_post_requirements.*')
			->first();

		$requirement = JobPostRequirement::where('id', '=', $requirement_db->id)->first();
		return $requirement;
	}

	public function getEmployerPosts($employer)
	{
		$emp_id = User::where('name', '=', $employer)->first();
		$EmpID = $emp_id->id;

		$jobposts = JobPost::where('user_id', '=', $EmpID)->pluck('title')->toArray();

		return $jobposts;
	}

	public function getPostNames()
	{
		$jobposts = JobPost::all();

		return $jobposts;
	}
}
