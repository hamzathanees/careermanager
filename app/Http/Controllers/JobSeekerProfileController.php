<?php

namespace App\Http\Controllers;

use App\EducationLevel;
use App\JobPost;
use App\JobPostRequirement;
use App\JobSeekerProfile;
use App\Resume;
use App\ResumeSkill;
use App\Skill;
use App\SkillRelation;
use Illuminate\Http\Request;
use DB;
use Auth;
use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use NlpTools\Stemmers\PorterStemmer;
use DateTime;

class JobSeekerProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

		JobSeekerProfile::find($id)->update([
			'initials' 			=> $request->input('initials'),
			'first_name' 		=> $request->input('first_name'),
			'last_name' 		=> $request->input('last_name'),
			'gender' 			=> $request->input('gender'),
			'location' 			=> $request->input('location'),
			'mobile' 			=> $request->input('contact_number'),
			'experience' 		=> $request->input('experience'),
			'industry' 			=> $request->input('industry'),
			'qualification' 	=> $request->input('qualification'),
		]);

		if (JobSeekerProfile::find($id)->update() == true)
		{
			flash()->overlay('Success', 'Profile Successfully Updated');
			return redirect()->back();
		}
		else
		{
			flash()->overlay('Error', 'Profile Update Failed!');
			return redirect()->back();
		}




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function resume($id)
	{
		$profile = JobSeekerProfile::find($id);
		$resume = Resume::where('job_seeker_profile_id', '=', $id)->first();
		return view('demo', compact('profile', 'resume'));
	}

	/**
	 * same as above the above is for testing
	 * @param $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function resumeNew($id)
	{
		$profile = JobSeekerProfile::find($id);
		$resume = Resume::where('job_seeker_profile_id', '=', $id)->first();
		return view('frontend.profiles.resume', compact('profile', 'resume'));
	}

	public function resumeData($id)
	{
		$resume = Resume::where('job_seeker_profile_id', '=', $id)->first();
		$profile = JobSeekerProfile::find($id);
//		$summary = Resume::selectRaw('summary')->where('job_seeker_profile_id', '=', $id)->first()->summary;
//		$skills = json_decode(Resume::selectRaw('skills as skills')->where('job_seeker_profile_id', '=', $id)->first()->skills);

//		$skills = $resume->skills;

		return $resume;

	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public function getResume($id)
	{

//		$resume = Resume::where('job_seeker_profile_id', '=', $id)->first();
		$resume = $this->resumeData($id);
//		$summary = Resume::selectRaw('summary')->where('job_seeker_profile_id', '=', $id)->first()->summary;
//		$skills = json_decode(Resume::selectRaw('skills as skills')->where('job_seeker_profile_id', '=', $id)->first()->skills);

//		$skills = $resume->skills;

		return view('frontend.profiles.viewresume', compact('resume'));

	}



	public function resumeSave(Request $request)
	{
//		$profile = JobSeekerProfile::find($id);
//		Resume::create($request->all());
		if(Auth::user()->job_seeker_profile->id == $request->input('job_seeker_profile_id'))
		{
			$skills = $request->input('skills');
			$experiences = $request->input('experiences');
			$job_seeker_profile_id = $request->input('job_seeker_profile_id');
			$summary = $request->input('summary');
			$education = $request->input('education');
			$references = $request->input('references');

			//return [$job_seeker_profile_id, $summary, $skills, $experiences, $education, $references];


			$resume = Resume::firstOrNew(array('job_seeker_profile_id' => $job_seeker_profile_id));
			//		$resume->job_seeker_profile_id = $job_seeker_profile_id;
			$resume->summary = $summary;
			$resume->skills = $skills;
			$resume->education = $education;
			$resume->experiences = $experiences;
			$resume->references = $references;
			$resume->save();


			if ($resume->save()){
				flash()->overlay('Success', 'Resume Successfully Updated');
				return $resume;
			}
		}

	}

	public function runExperiencePoints($job_post_id, $job_seeker_profile_id){
		$job_seeker_profile = JobSeekerProfile::find($job_seeker_profile_id);
		$job_post = JobPost::find($job_post_id);

		$resume = $this->getResumeData($job_seeker_profile_id);
		$requirement = JobPostRequirement::where('job_post_id', '=', $job_post_id)->first();

		$resume_exp_total = 0;
		foreach ($resume['cv-exp'] as $exp){
			$resume_exp_total += $exp['months'];
		}

		$highest_req = 0;
		foreach ($requirement['experiences'] as $exp){
			$temp = $exp['months'];
			if($temp > $highest_req){
				$highest_req = $temp;
			}
		}

		$exp_point = 0;
		if($resume_exp_total == (int)$highest_req){
			$exp_point = 30;

		}
		if($resume_exp_total > (int)$highest_req){
			$exp_point = 30 + ((($resume_exp_total - (int)$highest_req) / $resume_exp_total) * 5);
		}
		if($resume_exp_total < (int)$highest_req){
			$exp_point = 30 - (((int)$highest_req - $resume_exp_total) / 12);
		}

		DB::table('job_post_job_seeker_profile')
			->where([
				['job_seeker_profile_id', '=', $job_seeker_profile_id],
				['job_post_id', '=', $job_post_id],
			])
			->update(['experience_point' => $exp_point]);
	}


	public function applyJobPost(Request $request, $job_post_id, $job_seeker_profile_id)
	{
		if(Auth::user()->job_seeker_profile->id == $job_seeker_profile_id){
			$job_seeker_profile = JobSeekerProfile::find($job_seeker_profile_id);
			$job_post = JobPost::find($job_post_id);

			//		return [$job_post_id, $job_seeker_profile_id];


			//			$job_post->job_categories()->sync($jobPostRequest->categories, true);
			if (!$job_seeker_profile->applies->contains($job_post->id)){
				//			$job_seeker_profile->applies()->sync($job_post->id, true);



				//run the textmining function() just before the application is saved in the database


				$job_seeker_profile->applies()->attach($job_post_id);

				$resume = Resume::where('job_seeker_profile_id', '=', $job_seeker_profile_id)->first();
				$requirement = JobPostRequirement::where('job_post_id', '=', $job_post_id)->first();

				if($resume['skills'] != null && $resume['experiences'] != null
					&& $resume['education'] != null){

					if($requirement['skills'] != null &&
						$requirement['experiences'] != null
						&& $requirement['education'] != null){
						$this->runEducationPoints($job_post_id, $job_seeker_profile_id);
						$this->runSkillPoints($job_post_id, $job_seeker_profile_id);
						$this->runExperiencePoints($job_post_id, $job_seeker_profile_id);
					}
				}
			}
			//
			//
			flash()->overlay('Success', 'Job Post Applied Successfully!');

			return redirect()->back();
		}
	}

	public function runSkillPoints($job_id, $profile_id)
	{
		$resume_data = $this->getResumeData($profile_id);
		$requirement_data = $this->getRequirementData($job_id);

		$user_skills_all = $resume_data['cv-skills'];
		$user_skills_unique = array_unique($user_skills_all);
		$requirement_skills = $requirement_data['skills'];
		$skill_relations = SkillRelation::all();

		$skill_point = 0;

		$requirement_skills_array = array();
		$requirement_skills_array_name = array();
		$requirement_skills_array_id = array();
		foreach ($requirement_skills as $requirement_skill)
		{
			$skill = Skill::where('name', 'like', $requirement_skill)->first();
			array_push($requirement_skills_array, array('id' => $skill->id, 'name' => $skill->name));
			array_push($requirement_skills_array_name, $skill->name);
			array_push($requirement_skills_array_id, $skill->id);

		}


		foreach ($user_skills_unique as $user_skill)
		{

			if(in_array($user_skill, $requirement_skills_array_name))
			{
				$skill_point += 3;
			}

			$skill = Skill::where('name', 'like', $user_skill)->first();
			foreach ($skill_relations as $skill_relation)
			{
				if($skill_relation->related_skill_id == $skill->id
					&& in_array($skill_relation->skill_id, $requirement_skills_array_id))
				{
					$skill_point += 1;
				}
			}
		}

		DB::table('job_post_job_seeker_profile')
			->where([
				['job_seeker_profile_id', '=', $profile_id],
				['job_post_id', '=', $job_id],
			])
			->update(['skill_point' => $skill_point]);
	}

	public function runEducationPoints($job_id, $profile_id)
	{

		$resume_data = $this->getResumeData($profile_id);
		$requirement_data = $this->getRequirementData($job_id);

		$total_education_score = $this->getTotalEducationScore($resume_data, $requirement_data);

		DB::table('job_post_job_seeker_profile')
			->where([
				['job_seeker_profile_id', '=', $profile_id],
				['job_post_id', '=', $job_id],
				])
			->update(['education_point' => $total_education_score[0]]);
	}

	private function getTotalEducationScore($resume_data, $requirement_data){
		$user_education_level = array();

		foreach ($resume_data['cv-edu'] as $cveducation){
			$ed_level = EducationLevel::where('name', '=', $cveducation['name'])->first();
			array_push($user_education_level, $ed_level->id);
		}

		$req_edu_level = EducationLevel::where('name', '=', $requirement_data['education'][0]['name'])->pluck('level');


		$education_score = 0;
		$additional_score = 0;
		$temp_highest_level = 0;
		$count = 0;

		foreach ($user_education_level as $level){
			if($level > $temp_highest_level){
				$temp_highest_level = $level;
			}
		}

		if($temp_highest_level >= $req_edu_level[0]){
			$education_score = 30;
		}else{
			$education_score = 30 - ($req_edu_level[0] - $temp_highest_level);
		}


		if($education_score == 30){
			$additional_score = ((12 - $req_edu_level[0]) / (12 - $temp_highest_level)) ;
		}

		$total_ed_score = round(($education_score + $additional_score), 2);

		return array($total_ed_score, $temp_highest_level, $user_education_level);
	}

	private function getResumeData($profile_id)
	{
		$resume = Resume::where('job_seeker_profile_id', '=', $profile_id)->first();

		$cv_skills = array();
		foreach ($resume['skills'] as $skill){
			array_push($cv_skills, $skill['name']);
		}

		$cv_experiences = array();
		foreach ($resume['experiences'] as $experience){
			array_push($cv_experiences, array('name' => $experience['name']));
		}

		$obj_exp_array = array();
		foreach ($resume['experiences'] as $experience){
			$start_date = new DateTime($experience['start_date']);
			$end_date = new DateTime($experience['end_date']);

			array_push($obj_exp_array,
				array('name' => strtolower($experience['name']), 'months' =>
					floor($start_date->diff($end_date)->format("%a") * 0.0328549112)));
		}

		$obj_edu_array = array();
		foreach ($resume['education'] as $education){
			$start_date = new DateTime($education['start_date']);
			$end_date = new DateTime($education['end_date']);
			array_push($obj_edu_array,
				array('name' => strtolower($education['name']), 'months' =>
					floor($start_date->diff($end_date)->format("%a") * 0.0328549112) ));
		}

		return array(

			'cv-skills' => $cv_skills,

			'cv-exp' 	=> $obj_exp_array,

			'cv-edu' 	=> $obj_edu_array

		);
	}

	private function getRequirementData($job_id)
	{
		$job_post_requirement = JobPostRequirement::where('job_post_id', '=', $job_id)->first();

		$req_skills = array();
		foreach ($job_post_requirement['skills'] as $skill){
			array_push($req_skills, array('name' => $skill['name']));
		}


		$req_edu = array();
		foreach ($job_post_requirement['education'] as $education){
			array_push($req_edu,
				array('name' => strtolower($education['name'])));
		}

		$req_exp = array();
		foreach ($job_post_requirement['experiences'] as $experience){
			array_push($req_exp,
				array('months' => strtolower($experience['months'])));
		}

		return array('skills' => $req_skills, 'education' => $req_edu, 'experience' => $req_exp);
	}

	public function withdrawJobPost(Request $request, $job_post_id, $job_seeker_profile_id)
	{
		if(Auth::user()->job_seeker_profile->id == $job_seeker_profile_id)
		{
			$job_seeker_profile = JobSeekerProfile::find($job_seeker_profile_id);
			$job_post = JobPost::find($job_post_id);

			//		$job_post->job_categories()->sync($jobPostRequest->categories, true);
			if ($job_seeker_profile->applies->contains($job_post->id))
			{
				$job_seeker_profile->applies()->detach($job_post->id);
			}


			flash()->overlay('Success', 'Job Post Application withdrawn Successfully!');

			return redirect()->back();
		}
	}


}
