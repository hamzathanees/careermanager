<?php

namespace App\Http\Controllers;

use App\JobPost;
use Illuminate\Http\Request;
use Auth;

class JobPostCommentController extends Controller
{
    //
	public function save(Request $request, $jobpost)
	{
		//
		$commentedJobPost = JobPost::find($jobpost);
		$user_id = Auth::user()->id;
		$commentedJobPost->comments()->create([
			'comment' 		=> $request->input('comment'),
			'parent_id' 	=> $request->input('parent_id', null),
			'user_id'		=> $user_id,
			'job_post_id'	=> $jobpost,
		]);

		flash()->overlay('Success', 'Comment Successfully created!');

		return back();

	}
}
