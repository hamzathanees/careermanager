<?php

namespace App\Http\Controllers;

use App\EmployerProfile;
use App\JobSeekerProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Auth;

class ProfileController extends Controller
{
    //
	public function getProfile($slug)
	{
		$user_name 	= str_replace('_', ' ', $slug);
		$user 		= User::where('name', '=', $user_name)->first();

		if ($user->role->name == 'Job Seeker')
		{
			$profile = JobSeekerProfile::where('user_id', '=', $user->id)->first();
		}
		if ($user->role->name == 'Employer')
		{
			$profile = EmployerProfile::where('user_id', '=', $user->id)->first();
		}

		return view('frontend.profiles.profile', compact('profile'));

//		return $user;
	}

	public function updateAvatar(Request $request)
	{
		$this->validate($request, [
			'avatar' => 'required|image',
		]);

		if($request->hasFile('avatar')){
			$avatar = $request->file('avatar');
			$filename = 'images/avatars/' . Auth::user()->slug . '.' . 'png';
			Image::make($avatar)->resize(250, 250)->save( public_path($filename) );
			$user = Auth::user();
			$user->avatar = $filename;
			$user->save();
		}

		return back();

	}
}
