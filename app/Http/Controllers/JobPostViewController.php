<?php

namespace App\Http\Controllers;

use App\JobCategory;
use App\JobPost;
use App\JobPostRequirement;
use App\User;
use Illuminate\Http\Request;

use NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;

class JobPostViewController extends Controller
{
    //
	public function getSingle($slug)
	{
		//fetch the data
		$jobpost = JobPost::where('slug', '=', $slug)->first();

		$comments = $jobpost->comments->groupBy('parent_id');
		$comments['root'] = $jobpost->comments->where('parent_id', null)->all();

		$requirements = JobPostRequirement::where('job_post_id', '=', $jobpost->id)->first();
		$skills = $requirements->skills;
		$educations = $requirements->education;
		$experiences = $requirements->experiences;
//
		unset($comments['']);

		//return the view
		return view('frontend.frontendjobpost.single', compact('jobpost', 'comments', 'skills', 'educations', 'experiences'));

//		return $comments;
	}

	public function getSlugPerUser($user)
	{
		//fetch the data
		$jobpost = JobPost::where('user_id', '=', $user)->get();
		return $jobpost;
	}

	public function getAllForCategory($jobcategory)
	{

		$job_category = JobCategory::where('id', '=', $jobcategory)->first();

		$job_posts = $job_category->job_posts()->get();

		return view('jobcategory.posts', compact('job_posts'));
	}

	public function getSearch()
	{

	}
}
