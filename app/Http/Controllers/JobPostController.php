<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobPostRequest;
use App\JobCategory;
use App\JobPost;
use App\JobPostRequirement;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Purifier;
use DB;
class JobPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		$jobposts = JobPost::all();
		return view('jobpost.index', compact('jobposts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		$categories = JobCategory::all();
		return view('jobpost.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobPostRequest $jobPostRequest)
    {
        //
		$userid = Auth::user()->id;
		$title = $jobPostRequest->input('title');
		$description = $jobPostRequest->input('description');
		$job_post = JobPost::create([
			'title' 		=> $title,
			'description' 	=> Purifier::clean($description, 'youtube'),
			'slug' 			=> Str::slug($title, '_'),
			'due_date' 		=> $jobPostRequest->input('due_date'),
			'user_id'		=> $userid,
		]);

		$job_post->job_categories()->sync($jobPostRequest->categories, false);

		$job_post_requirement = new JobPostRequirement();
		$job_post_requirement->job_post_id = $job_post->id;
		$job_post_requirement->save();

		flash()->overlay('Success', 'Job Post Successfully created!');
		return redirect()->route('jobpost.show', $job_post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$jobpost = JobPost::find($id);
		return view('jobpost.show', compact('jobpost'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$jobpost = JobPost::find($id);
		$categories = JobCategory::all();

		if (Auth::user()->id == $jobpost->user_id || Auth::user()->role->name == 'Admin'){
			return view('jobpost.edit', compact('jobpost', 'categories'));
		}else{
			return redirect()->route('homepage');
		}

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobPostRequest $jobPostRequest, $id)
    {
        //
		$userid = Auth::user()->id;
		$job_post = JobPost::find($id);
		$title = $jobPostRequest->input('title');
		$description = $jobPostRequest->input('description');

		if (Auth::user()->id == $job_post->user_id || Auth::user()->role->name == 'Admin')
		{
			JobPost::find($id)->update([
				'title' 		=> $title,
				'description' 	=> Purifier::clean($description, 'youtube'),
				'slug' 			=> Str::slug($title, '_'),
				'due_date' 		=> $jobPostRequest->input('due_date'),
				'user_id'		=> $userid,
			]);



			$job_post->job_categories()->sync($jobPostRequest->categories, true);
			//true or without false means delete previous and save the new one

			flash()->overlay('Success', 'Job Post Successfully updated!');
			return redirect()->route('jobpost.show', $id);
		}else
		{
			return redirect()->route('homepage');
		}


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		$jobpost = JobPost::find($id);

		if (Auth::user()->id == $jobpost->user_id || Auth::user()->role->name == 'Admin'){
			$jobpost->job_categories()->detach();

			$jobpost->delete();

			flash()->overlay('Success', 'Job Post Successfully deleted!');
			return redirect()->route('jobpost.index');
		}else{
			return redirect()->route('homepage');
		}

    }


    public function getRank($job_post_id){
    	$ranks = DB::table('job_post_job_seeker_profile')
			->where('job_post_id', '=', $job_post_id)
			->get();

    	return view('frontend.rank.index', compact('ranks'));
	}
}
