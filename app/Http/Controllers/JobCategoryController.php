<?php

namespace App\Http\Controllers;

use App\Http\Requests\JobCategoryRequest;
use App\JobCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jobcategories = JobCategory::all();
        return view('backend.admin.jobcategory.index', compact('jobcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.admin.jobcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JobCategoryRequest $categoryRequest
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Request $request)
    {
        //
        $job_category = JobCategory::create([
            'name' => $request->input('name'),
            'details' => $request->input('details'),
        ]);

        return redirect()->route('jobcategory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $jobcat = JobCategory::where('id', '=', $id)->first();
        return view('backend.admin.jobcategory.edit', compact('jobcat'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        JobCategory::find($id)->update($request->all());
        return redirect()->route('jobcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = JobCategory::find($id);

        $category->job_posts()->detach();

        $category->delete();
		flash()->overlay('Job Category Delete!', 'Job Category Deleted Successfully', 'error');
        return redirect()->route('jobcategory.index');
    }
}
