<?php

namespace App\Http\Controllers\Auth;

use App\EmployerProfile;
use App\JobSeekerProfile;
use App\Resume;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Hash;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    	$user_avatar = 'public/images/avatars/profiles.jpg';
    	$user_name 	= $data['name'];

		$user = User::create([
			'name' 		=> $data['name'],
			'email' 	=> $data['email'],
			'role_id' 	=> $data['role'],
			'slug' 		=> Str::slug($user_name, '_'),
			'password' 	=> Hash::make($data['password']), //use bcrypt method to hash the password.. but i didnt use it because we need the same string password to login into java application as well
			'avatars' 	=> $user_avatar,
		]);


        if ($user->role->name == 'Employer')
		{
			$employer_profile 				= new EmployerProfile();
			$employer_profile->user_id		= $user->id;
			$employer_profile->save();
		}

		if ($user->role->name == 'Job Seeker')
		{
			$job_seeker_profile 			= new JobSeekerProfile();
			$job_seeker_profile->user_id	= $user->id;
			$job_seeker_profile->save();

			$job_seeker_profile_resume 		= new Resume();
			$job_seeker_profile_resume->job_seeker_profile_id = $job_seeker_profile->id;
			$job_seeker_profile_resume->save();
		}

		return $user;
    }
}
