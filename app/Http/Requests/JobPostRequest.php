<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		if ($this->method() == 'PUT')
		{
			// Update operation, exclude the record with id from the validation:
			$title_rule 	= 'required|min:5|max:350|unique:job_posts,title,' . $this->get('id');
			$desc_rule 		= 'required|min:10';
		}
		else
		{
//			$start_date =
			// Create operation. There is no id yet.
			$title_rule 	= 'required|min:5|max:350|unique:job_posts,title';
			$desc_rule 		= 'required|min:10';
		}
		return [
			'title'     	=> $title_rule,
			'description'   => $desc_rule,
			'due_date'		=> 'required|after:today',
			'categories'	=> 'required',
		];
    }


	/**
	 * @return array
	 */
	public function messages()
	{
		return [
			'title.required' 				=> 'Job Post title is required',
			'title.min' 					=> 'Job Post title must have more than 5 characters',
			'title.alpha_numeric_spaces' 	=> 'Job Post title must have alpha numeric characters with spaces only',
			'title.unique' 					=> 'Job Post title already taken',
			'title.max'   					=> 'Job Post Title must not have more than 50 characters',
			'description.required' 			=> 'Job Post Description Field is required',
			'description.min'      			=> 'Job Post Description must have more than 10 characters',
			'due_date.required'      		=> 'Job Post Due Date is required',
		];
	}
}
