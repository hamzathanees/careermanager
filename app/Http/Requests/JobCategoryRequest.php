<?php

namespace App\Http\Requests;

use App\JobCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class JobCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        if (Auth::user()->role->rolename == 'Admin')
//        {
//            return true;
//        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->method() == 'PUT')
        {
            // Update operation, exclude the record with id from the validation:
            $name_rule = 'required|unique:job_categories,name,' . $this->get('id');
        }
        else
        {
            // Create operation. There is no id yet.
            $name_rule = 'required|unique:job_categories,name';
        }
        return [
            'name'     => $name_rule,
            'details'   => 'required|min:10',
        ];

    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Job Category Name Field is required',
            'name.unique' => 'Job Category Name already taken',
            'details.required' => 'Job Category Details Field is required',
            'name.max'      => 'Job Category Name cannot have more than 30 characters',
            'details.min'      => 'Job Category Details must have more than 10 characters',
        ];
    }
}
