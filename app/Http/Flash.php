<?php

namespace App\Http;

class Flash
{
    /**
     * create a flash message
     *
     * @param $title
     * @param $message
     * @param $type
     * @param string $key
     */
    public function create($title, $message, $type, $key = 'flash_message')
    {
        session()->flash($key, [
            'title' => $title,
            'message' => $message,
            'type' => $type
        ]);
    }

    /**
     * create an info type flash message
     *
     * @param $title
     * @param $message
     */
    public function info($title, $message)
    {
        return $this->create($title, $message, 'info');
    }

    /**
     * create a success type flash message
     *
     * @param $title
     * @param $message
     */
    public function success($title, $message)
    {
        return $this->create($title, $message, 'success');
    }

    /**
     * create an error type flash message
     *
     * @param $title
     * @param $message
     */
    public function error($title, $message)
    {
        return $this->create($title, $message, 'error');
    }

    /**
     * create an overlay type flash message
     *
     * @param $title
     * @param $message
     * @param string $type
     */
    public function overlay($title, $message, $type = 'success')
    {
        return $this->create($title, $message, $type, 'flash_message_overlay');
    }
}